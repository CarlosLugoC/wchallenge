/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.AddressDTO;
import ar.com.wolox.demo.dto.CompanyDTO;
import ar.com.wolox.demo.dto.GeoDTO;
import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class UserServiceImpTest {
    
    private static final String URL = "https://jsonplaceholder.typicode.com/users";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String OBJECT = "user";
    
    @Mock
    private RestTemplate restTemplate;

    @Mock
    private MessagesInternationalizationImp msg;
    
    @InjectMocks
    private UserServiceImp userService;
    
    @Before
    public void setUp() {
        
    }
    
    /**
     * Test of getAllUsers method, of class UserServiceImp.
     */
    @Test
    public void testGetAllUsers() {
        List<UserDTO> users = new ArrayList<>();
        UserDTO userTest = new UserDTO();
        userTest.setId(1);
        userTest.setPhone("312312");
        userTest.setEmail("email@email.com");
        userTest.setUsername("username");
        userTest.setWebsite("website");
        users.add(userTest);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<UserDTO>>() {}))
                .thenReturn(new ResponseEntity<>(users, HttpStatus.OK));
        List<UserDTO> response = userService.getAllUsers();
        Assert.assertNotNull(response);
        Assert.assertEquals(response.get(0).getId(), userTest.getId());
    }
    
    @Test(expected = BadRequestException.class)
    public void testGetAllUsersBadRequestException() {
        try {
            UserDTO userTest = new UserDTO();
            userTest.setId(1);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<UserDTO>>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            List<UserDTO> response = userService.getAllUsers();
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of getUserById method, of class UserServiceImp.
     */
    @Test
    public void testGetUserById() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        String url = String.format(STRINGFORMATSLASH,URL,userTest.getId());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
        UserDTO response = userService.getUserById(1);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getAddress(), userTest.getAddress());
    }
    
    @Test(expected = NotFoundException.class)
    public void testGetUserByIdNotFoundException() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        userTest.setCompany(company);
        userTest.setAddress(address);
        try {
            String url = String.format(STRINGFORMATSLASH,URL,userTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            UserDTO response = userService.getUserById(userTest.getId());
       } catch(RestClientException ex) {
           throw new NotFoundException(msg.msgNotFound(OBJECT, userTest.getId()));
       }
    }

    /**
     * Test of createUser method, of class UserServiceImp.
     */
    @Test
    public void testCreateUser() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        List<UserDTO> users = new ArrayList<>();
        HttpEntity<UserDTO> request = new HttpEntity<>(userTest);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<UserDTO>>() {}))
                .thenReturn(new ResponseEntity<>(users, HttpStatus.OK));
        when(restTemplate.exchange(
                URL, HttpMethod.POST, request, new ParameterizedTypeReference<UserDTO>() {}))
                .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
        UserDTO response = userService.createUser(userTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response, userTest);
    }
    
    @Test(expected = ConflictException.class)
    public void testCreateUserCreatedException() {
        try {
            GeoDTO geo = new GeoDTO("lat", "lng");
            AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
            CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
            UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
            List<UserDTO> users = new ArrayList<>();
            users.add(userTest);
            HttpEntity<UserDTO> request = new HttpEntity<>(userTest);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<UserDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(users, HttpStatus.OK));
            when(restTemplate.exchange(
                    URL, HttpMethod.POST, request, new ParameterizedTypeReference<UserDTO>() {}))
                    .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
            UserDTO response = userService.createUser(userTest);
        } catch(RestClientException ex){
            throw new ConflictException(msg.msgConflict(OBJECT));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testCreateBadRequestException() {
        try {
            GeoDTO geo = new GeoDTO("lat", "lng");
            AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
            CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
            UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
            List<UserDTO> users = new ArrayList<>();
            HttpEntity<UserDTO> request = new HttpEntity<>(userTest);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<UserDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(users, HttpStatus.OK));
            when(restTemplate.exchange(
                    URL, HttpMethod.POST, request, new ParameterizedTypeReference<UserDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            UserDTO response = userService.createUser(userTest);
        } catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }

    /**
     * Test of modifyUser method, of class UserServiceImp.
     */
    @Test
    public void testUpdateUser() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        String url = String.format(STRINGFORMATSLASH,URL,userTest.getId());
        HttpEntity<UserDTO> request = new HttpEntity<>(userTest);
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
        when(restTemplate.exchange(
                url, HttpMethod.PUT, request, new ParameterizedTypeReference<UserDTO>() {}))
                .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
        UserDTO response = userService.updateUser(userTest.getId(),userTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getCompany(), userTest.getCompany());
    }
    
    @Test(expected = NotFoundException.class)
    public void testUpdateUserNotFoundException() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        try {
            UserDTO userNull = null;
            String url = String.format(STRINGFORMATSLASH,URL,userTest.getId());
            HttpEntity<UserDTO> request = new HttpEntity<>(userTest);
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                    .thenReturn(new ResponseEntity<>(userNull, HttpStatus.OK));
            when(restTemplate.exchange(
                    url, HttpMethod.PUT, request, new ParameterizedTypeReference<UserDTO>() {}))
                    .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
            UserDTO response = userService.updateUser(userTest.getId(),userTest);
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, userTest.getId()));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testUpdateUserBadRequestException() {
        try {
            GeoDTO geo = new GeoDTO("lat", "lng");
            AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
            CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
            UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
            String url = String.format(STRINGFORMATSLASH,URL,userTest.getId());
            HttpEntity<UserDTO> request = new HttpEntity<>(userTest);
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                    .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
            when(restTemplate.exchange(
                    url, HttpMethod.PUT, request, new ParameterizedTypeReference<UserDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            UserDTO response = userService.updateUser(userTest.getId(),userTest);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
    }

    /**
     * Test of deleteUserById method, of class UserServiceImp.
     */
    @Test
    public void testDeleteUserById() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        String url = String.format(STRINGFORMATSLASH,URL,userTest.getId());
        String msgR = "OK";
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
        this.restTemplate.delete(url);
        when(msg.msgSuccesfullyDelete(OBJECT, userTest.getId())).thenReturn(msgR);
        String response = userService.deleteUserById(userTest.getId());
        Assert.assertNotNull(response);
    }
    
    @Test(expected = NotFoundException.class)
    public void testDeleteUserByIdNotFoundException() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        try {
            UserDTO userNull = null;
            String url = String.format(STRINGFORMATSLASH,URL,userTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                    .thenReturn(new ResponseEntity<>(userNull, HttpStatus.OK));
            this.restTemplate.delete(url);
            String response = userService.deleteUserById(userTest.getId());
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, userTest.getId()));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testDeleteUserByIdBadRequestException() {
        try {
            GeoDTO geo = new GeoDTO("lat", "lng");
            AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
            CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
            UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
            String url = String.format(STRINGFORMATSLASH,URL,userTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                    .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
            this.restTemplate.delete(url);
            when(userService.deleteUserById(userTest.getId())).
                    thenThrow(new RestClientException(OBJECT));
            String response = userService.deleteUserById(userTest.getId());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
}
