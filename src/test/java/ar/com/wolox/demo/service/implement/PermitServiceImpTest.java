/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.AddressDTO;
import ar.com.wolox.demo.dto.AlbumDTO;
import ar.com.wolox.demo.dto.CompanyDTO;
import ar.com.wolox.demo.dto.GeoDTO;
import ar.com.wolox.demo.dto.PermitDTO;
import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import ar.com.wolox.demo.model.EPermission;
import ar.com.wolox.demo.model.Permit;
import ar.com.wolox.demo.repository.PermitRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class PermitServiceImpTest {
    
    private static final String OBJECT = "permit";
    private static final String OBJECTPERMISSIONS = "permissions";
    
    @Mock
    private PermitRepository permitRepository;
    
    @Mock
    private AlbumServiceImp albumService;
    
    @Mock
    private ModelMapper modMapper;
    
    @Mock
    private UserServiceImp userService;
    
    @Mock
    private MessagesInternationalizationImp msg;
    
    @InjectMocks
    private PermitServiceImp permitService;

    @Before
    public void setUp() {
    }
    

    /**
     * Test of registerAlbumPermission method, of class PermitServiceImp.
     */
    @Test
    public void testCreatePermit() {
        Set<EPermission> permissions = new HashSet<>();
        Set<String> permissionsDTO = new HashSet<>();
        permissionsDTO.add("WRITE");
        permissions.add(EPermission.WRITE);
        Permit permitTest = new Permit();
        permitTest.setId(1);
        permitTest.setAlbumId(1);
        permitTest.setPermissions(permissions);
        permitTest.setUserId(1);
        PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
        Permit permitNull = null;
        Optional<Permit> isPermit = Optional.ofNullable(permitNull);
        UserDTO userTest = new UserDTO();
        userTest.setId(1);
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
        when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
        when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(),permitDTOTest.getAlbumId())).thenReturn(isPermit);
        when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
        when(permitRepository.save(modMapper.map(permitDTOTest, Permit.class))).thenReturn(permitTest);
        when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
        PermitDTO response = permitService.createPermit(permitDTOTest);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(response.getPermissions(), permitTest.getPermissions());
    }
    
    @Test(expected = ConflictException.class)
    public void testCreatePermitCreatedException() {
        try {
            Set<EPermission> permissions = new HashSet<>();
            Set<String> permissionsDTO = new HashSet<>();
            permissions.add(EPermission.WRITE);
            Permit permitTest = new Permit(1, 1, permissions, 1);
            PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
            Optional<Permit> isPermit = Optional.ofNullable(permitTest);
            UserDTO userTest = new UserDTO();
            userTest.setId(1);
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
            when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
            when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(),permitDTOTest.getAlbumId())).thenReturn(isPermit);
            when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
            when(permitRepository.save(modMapper.map(permitDTOTest, Permit.class))).thenReturn(permitTest);
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            PermitDTO response = permitService.createPermit(permitDTOTest);
        } catch(RestClientException ex) {
            throw new ConflictException(msg.msgConflict(OBJECT));
        }

    }
    
    @Test(expected = BadRequestException.class)
    public void testCreatePermitBadRequestException() {
        try {
            Set<EPermission> permissions = new HashSet<>();
            Set<String> permissionsDTO = new HashSet<>();
            permissions.add(EPermission.WRITE);
            Permit permitTest = new Permit(1, 1, permissions, 1);
            PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
            Permit permitNull = null;
            Optional<Permit> isPermit = Optional.ofNullable(permitNull);
            UserDTO userTest = new UserDTO();
            userTest.setId(1);
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
            when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
            when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(),permitDTOTest.getAlbumId())).thenReturn(isPermit);
            when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
            when(permitRepository.save(modMapper.map(permitDTOTest, Permit.class))).thenThrow(new RestClientException(OBJECT));
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            PermitDTO response = permitService.createPermit(permitDTOTest);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }
    

    /**
     * Test of changeAlbumPermission method, of class PermitServiceImp.
     */
    @Test
    public void testUpdatePermit() {
        Set<EPermission> permissions = new HashSet<>();
        Set<String> permissionsDTO = new HashSet<>();
        permissionsDTO.add("WRITE");
        permissions.add( EPermission.WRITE);
        Permit permitTest = new Permit(1, 1, permissions, 1);
        PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
        Optional<Permit> isPermit = Optional.ofNullable(permitTest);
        UserDTO userTest = new UserDTO();
        userTest.setId(1);
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
        when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
        when(permitRepository.findById(permitDTOTest.getUserId())).thenReturn(isPermit);
        when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(), permitDTOTest.getAlbumId())).thenReturn(isPermit);
        when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
        when(permitRepository.save(permitTest)).thenReturn(permitTest);
        when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
        PermitDTO response = permitService.updatePermit(permitDTOTest.getId(),permitDTOTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getAlbumId(), permitTest.getAlbumId());
    }
    
    @Test(expected = NotFoundException.class)
    public void testUpdatePermitNotFoundException() {
        Set<String> permissionsDTO = new HashSet<>();
        permissionsDTO.add("WRITE");
        PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
        try {
            Set<EPermission> permissions = new HashSet<>();
            permissions.add(EPermission.WRITE);
            Permit permitTest = new Permit(1, 1, permissions, 1);
            Permit permitNull = null;
            Optional<Permit> isPermit = Optional.ofNullable(permitNull);
            UserDTO userTest = new UserDTO();
            userTest.setId(1);
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
            when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
            when(permitRepository.findById(permitDTOTest.getUserId())).thenReturn(isPermit);
            when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(), permitDTOTest.getAlbumId())).thenReturn(isPermit);
            when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
            when(permitRepository.save(permitTest)).thenReturn(permitTest);
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            PermitDTO response = permitService.updatePermit(permitDTOTest.getId(),permitDTOTest);
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, permitDTOTest.getId()));
        }
    }
    
    @Test(expected = ConflictException.class)
    public void testUpdatePermitConflictException() {
        try {
            Set<EPermission> permissions = new HashSet<>();
            Set<String> permissionsDTO = new HashSet<>();
            permissionsDTO.add("WRITE");
            permissions.add(EPermission.WRITE);
            Permit permitTest = new Permit(1, 1, permissions, 1);
            PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
            Permit permitException = new Permit(2, 2, permissions, 2);
            Optional<Permit> isPermit = Optional.ofNullable(permitTest);
            UserDTO userTest = new UserDTO();
            userTest.setId(1);
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
            when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
            Optional<Permit> isPermitWithAnotherAlbumIdAndPermission = Optional.ofNullable(permitException);
            when(permitRepository.findById(permitDTOTest.getUserId())).thenReturn(isPermit);
            when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(), permitDTOTest.getAlbumId())).
                    thenReturn(isPermitWithAnotherAlbumIdAndPermission);
            when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
            when(permitRepository.save(permitTest)).thenReturn(permitTest);
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            PermitDTO response = permitService.updatePermit(permitDTOTest.getId(),permitDTOTest);
        } catch(RestClientException ex) {
            throw new ConflictException(msg.msgConflict(OBJECT));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testUpdatePermitBadRequestException() {
        try {
            Set<EPermission> permissions = new HashSet<>();
            Set<String> permissionsDTO = new HashSet<>();
            permissionsDTO.add("WRITE");
            permissions.add(EPermission.WRITE);
            Permit permitTest = new Permit(1, 1, permissions, 1);
            PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
            Optional<Permit> isPermit = Optional.ofNullable(permitTest);
            UserDTO userTest = new UserDTO();
            userTest.setId(1);
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
            when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
            when(permitRepository.findById(permitDTOTest.getUserId())).thenReturn(isPermit);
            when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(), permitDTOTest.getAlbumId())).thenReturn(isPermit);
            when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
            when(permitRepository.save(permitTest)).thenThrow(new RestClientException(OBJECT));
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            PermitDTO response = permitService.updatePermit(permitDTOTest.getId(),permitDTOTest);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
    }
    
    @Test(expected = NotFoundException.class)
    public void testUpdatePermitNotFoundExceptionPermissionMapped() {
        Set<String> permissionsDTO = new HashSet<>();
        String permissionString = "WATCH";
        permissionsDTO.add(permissionString);
        PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
        try {
            Set<EPermission> permissions = new HashSet<>();
            permissions.add(EPermission.WRITE);
            Permit permitTest = new Permit(1, 1, permissions, 1);
            Optional<Permit> isPermit = Optional.ofNullable(permitTest);
            UserDTO userTest = new UserDTO();
            userTest.setId(1);
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
            when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
            when(permitRepository.findById(permitDTOTest.getUserId())).thenReturn(isPermit);
            when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(), permitDTOTest.getAlbumId())).thenReturn(isPermit);
            when(modMapper.map(permissionString, EPermission.class)).thenThrow(new RestClientException(OBJECT));
            when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
            when(permitRepository.save(permitTest)).thenReturn(permitTest);
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            PermitDTO permissionS = permitService.updatePermit(permitDTOTest.getId(),permitDTOTest);
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT,permitDTOTest.getUserId() ));
        }
    }
    
    @Test
    public void testUpdatePermitPermissionMapped() {
        Set<String> permissionsDTO = new HashSet<>();
        String permissionString = "WRITE";
        permissionsDTO.add(permissionString);
        PermitDTO permitDTOTest = new PermitDTO(1, 1, permissionsDTO, 1);
        try {
            Set<EPermission> permissions = new HashSet<>();
            
            permissions.add(EPermission.WRITE);
            Permit permitTest = new Permit(1, 1, permissions, 1);
            Optional<Permit> isPermit = Optional.ofNullable(permitTest);
            UserDTO userTest = new UserDTO();
            userTest.setId(1);
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            when(userService.getUserById(permitDTOTest.getUserId())).thenReturn(userTest);
            when(albumService.getAlbumById(permitDTOTest.getAlbumId())).thenReturn(albumTest);
            when(permitRepository.findById(permitDTOTest.getUserId())).thenReturn(isPermit);
            when(permitRepository.findByUserIdAndAlbumId(permitDTOTest.getUserId(), permitDTOTest.getAlbumId())).thenReturn(isPermit);
            when(modMapper.map(permissionString, EPermission.class)).thenThrow(new RestClientException(OBJECT));
            when(modMapper.map(permitDTOTest, Permit.class)).thenReturn(permitTest);
            when(permitRepository.save(permitTest)).thenReturn(permitTest);
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            PermitDTO permissionS = permitService.updatePermit(permitDTOTest.getId(),permitDTOTest);
            Assert.assertNotNull(permissionS);
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, permitDTOTest.getId()));
        }
    }

    /**
     * Test of usersIdByPermission method, of class PermitServiceImp.
     */
    @Test
    public void testGetUsersIdByAlbumIdAndPermission() {
        List<Permit> permitListTest = new ArrayList<>();
        Set<EPermission> permissions = new HashSet<>();
        permitListTest.add(new Permit(1, 2, permissions, 2));
        permissions.add(EPermission.WRITE);
        String permissionString = "WRITE";
        when(permitRepository.findByAlbumIdAndPermissionsIn(permitListTest.get(0).getAlbumId(), permissions)).thenReturn(permitListTest);
        List<Integer> response = permitService.getUsersIdByAlbumIdAndPermission(permitListTest.get(0).getAlbumId(), permissionString);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.get(0), permitListTest.get(0).getUserId());
    }
    
    
    @Test(expected = BadRequestException.class)
    public void testGetUsersIdByAlbumIdAndPermissionBadRequestException() {
        try {
            List<Permit> permissionTest = new ArrayList<>();
            Set<EPermission> permissions = new HashSet<>();
            permissionTest.add(new Permit(1, 2, permissions, 2));
            permissions.add(EPermission.WRITE);
            String permissionString = "WRITE";
            when(permitRepository.findByAlbumIdAndPermissionsIn(permissionTest.get(0).getAlbumId(), permissions)).thenThrow(new RestClientException(OBJECT));
            List<Integer> response = permitService.getUsersIdByAlbumIdAndPermission(permissionTest.get(0).getAlbumId(), permissionString);
        } catch (RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of usersByPermission method, of class PermitServiceImp.
     */
    @Test
    public void testGetUsersByAlbumIdAndPermission() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("calle", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "prueba", "prueba@mail.com", address, "154454", "www", company);
        List<Permit> permitListTest = new ArrayList<>();
        Set<EPermission> permissions = new HashSet<>();
        permitListTest.add(new Permit(1, 2, permissions, 1));
        permissions.add(EPermission.WRITE);
        String permissionString = "WRITE";
        when(permitRepository.findByAlbumIdAndPermissionsIn(permitListTest.get(0).getAlbumId(), permissions)).thenReturn(permitListTest);
        when(userService.getUserById(userTest.getId())).thenReturn(userTest);
        List<UserDTO> response = permitService.getUsersByAlbumIdAndPermission(permitListTest.get(0).getAlbumId(), permissionString);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.get(0), userTest);

    }
    
    @Test(expected = BadRequestException.class)
    public void testGetUsersByAlbumIdAndPermissionBadRequestException() {
        try{
            GeoDTO geo = new GeoDTO("lat", "lng");
            AddressDTO address = new AddressDTO("calle", "suite", "Pereira", "660001", geo);
            CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
            UserDTO userTest = new UserDTO(1, "prueba", "prueba@mail.com", address, "154454", "www", company);
            List<Permit> permitListTest = new ArrayList<>();
            Set<EPermission> permissions = new HashSet<>();
            Permit permitTest = new Permit(1, 2, permissions, 2);
            permitListTest.add(permitTest);
            permissions.add(EPermission.WRITE);
            String permissionString = "WRITE";
            when(permitRepository.findByAlbumIdAndPermissionsIn(permitTest.getAlbumId(), permissions)).thenReturn(permitListTest);
            when(userService.getUserById(userTest.getId())).thenReturn(userTest);
            when(permitService.getUsersByAlbumIdAndPermission(permitTest.getAlbumId(), permissionString)).thenThrow(new RestClientException(OBJECT));
            List<UserDTO> response = permitService.getUsersByAlbumIdAndPermission(permitTest.getAlbumId(), permissionString);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of getAllPermissions method, of class PermitServiceImp.
     */
    @Test
    public void testGetAllPermits() {
        List<Permit> permitListTest = new ArrayList<>();
        Set<EPermission> permissions = new HashSet<>();
        Permit permitTest = new Permit(1, 2, permissions, 2);
        permitListTest.add(permitTest);
        List<PermitDTO> permitDTOListTest = new ArrayList<>();
        Set<String> permissionsDTO = new HashSet<>();
        PermitDTO permitDTO = new PermitDTO(1, 2, permissionsDTO, 2);
        permitDTOListTest.add(permitDTO);
        Page<Permit> responsePage = new PageImpl<>(permitListTest);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        Pageable paging = PageRequest.of(pageNumber, pageSize);
        when(permitRepository.findAll(paging)).thenReturn(responsePage);
        when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTO);
        Page<PermitDTO> response = permitService.getAllPermits(pageNumber,pageSize);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(response, responsePage);
    }

    @Test(expected = BadRequestException.class)
    public void testGetAllPermitsBadRequestException() {
        try {
            Set<EPermission> permissions = new HashSet<>();
            Permit permitTest = new Permit(1, 2, permissions, 2);
            Set<String> permissionsDTO = new HashSet<>();
            PermitDTO permitDTO = new PermitDTO(1, 2, permissionsDTO, 2);
            Integer pageNumber = 0;
            Integer pageSize = 3;
            Pageable paging = PageRequest.of(pageNumber, pageSize);
            when(permitRepository.findAll(paging)).thenThrow(new RestClientException(OBJECT));
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTO);
            Page<PermitDTO> response = permitService.getAllPermits(pageNumber,pageSize);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }
    /**
     * Test of getPermissionsByUser method, of class PermitServiceImp.
     */
    @Test
    public void testGetPermitsByUser() {
        List<Permit> permitListTest = new ArrayList<>();
        Set<EPermission> permissions = new HashSet<>();
        Set<String> permissionsDTO = new HashSet<>();
        Permit permitTest = new Permit(1, 2, permissions, 2);
        PermitDTO permitDTOTest = new PermitDTO(1, 2, permissionsDTO, 2);
        permitListTest.add(permitTest);
        Page<Permit> responsePage = new PageImpl<>(permitListTest);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        Pageable paging = PageRequest.of(pageNumber, pageSize);
        when(permitRepository.findByUserId(permitListTest.get(0).getUserId(),paging)).thenReturn(responsePage);
        when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
        Page<PermitDTO> response = permitService.getPermitsByUser(permitListTest.get(0).getUserId(),pageNumber,pageSize);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(response.get(), responsePage.get());

    }
    
    @Test(expected = BadRequestException.class)
    public void testGetPermitsByUserBadRequestException() {
        try {
            List<Permit> permitListTest = new ArrayList<>();
            Set<EPermission> permissions = new HashSet<>();
            Set<String> permissionsDTO = new HashSet<>();
            Permit permitTest = new Permit(1, 2, permissions, 2);
            permitListTest.add(permitTest);
            PermitDTO permitDTOTest = new PermitDTO(1, 2, permissionsDTO, 2);
            Page<Permit> responsePage = new PageImpl<>(permitListTest);
            Integer pageNumber = 0;
            Integer pageSize = 3;
            Pageable paging = PageRequest.of(pageNumber, pageSize);
            when(permitRepository.findByUserId(permitListTest.get(0).getUserId(),paging)).thenThrow(new RestClientException(OBJECT));
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            Page<PermitDTO> response = permitService.getPermitsByUser(permitListTest.get(0).getUserId(),pageNumber,pageSize);
        } catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }

    }

    /**
     * Test of getPermissionsByAlbum method, of class PermitServiceImp.
     */
    @Test
    public void testGetPermitsByAlbum() {
        List<Permit> permitListTest = new ArrayList<>();
        Set<EPermission> permissions = new HashSet<>();
        Set<String> permissionsString = new HashSet<>();
        Permit permitTest = new Permit(1, 2, permissions, 2);
        PermitDTO permitDTOTest = new PermitDTO(1, 2, permissionsString, 2);
        permitListTest.add(permitTest);
        Page<Permit> responsePage = new PageImpl<>(permitListTest);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        Pageable paging = PageRequest.of(pageNumber, pageSize);
        when(permitRepository.findByAlbumId(permitListTest.get(0).getAlbumId(),paging)).thenReturn(responsePage);
        when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
        Page<PermitDTO> response = permitService.getPermitsByAlbum(permitListTest.get(0).getAlbumId(),pageNumber,pageSize);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(response.get(), responsePage.get());

    }
    
    @Test(expected = BadRequestException.class)
    public void testGetPermitsByBadRequestException() {
        try {
            List<Permit> permitListTest = new ArrayList<>();
            Set<EPermission> permissions = new HashSet<>();
            Set<String> permissionsString = new HashSet<>();
            Permit permitTest = new Permit(1, 2, permissions, 2);
            PermitDTO permitDTOTest = new PermitDTO(1, 2, permissionsString, 2);
            permitListTest.add(permitTest);
            Integer pageNumber = 0;
            Integer pageSize = 3;
            Pageable paging = PageRequest.of(pageNumber, pageSize);
            when(permitRepository.findByAlbumId(permitListTest.get(0).getAlbumId(),paging)).thenThrow(new RestClientException(OBJECT));
            when(modMapper.map(permitTest, PermitDTO.class)).thenReturn(permitDTOTest);
            Page<PermitDTO> response = permitService.getPermitsByAlbum(permitListTest.get(0).getAlbumId(),pageNumber,pageSize);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of deletePermissionById method, of class PermitServiceImp.
     */
    @Test
    public void testDeletePermitById() {
        Set<EPermission> permissions = new HashSet<>();
        String msgR = "OK";
        Permit permitTest = new Permit(1, 1, permissions, 1);
        Optional<Permit> isPermit = Optional.ofNullable(permitTest);
        when(permitRepository.findById(permitTest.getId())).thenReturn(isPermit);
        this.permitRepository.deleteById(permitTest.getId());
        when(msg.msgSuccesfullyDelete(OBJECT, permitTest.getId())).thenReturn(msgR);
        String response = permitService.deletePermitById(permitTest.getId());
        Assert.assertNotNull(response);
    }
    
    @Test(expected = NotFoundException.class)
    public void testDeletePermitByIdNotFoundException() {
        Set<EPermission> permissions = new HashSet<>();
        Permit permitTest = new Permit(1, 1, permissions, 1);
        try {
            Permit permitNull = null;
            Optional<Permit> isPermit = Optional.ofNullable(permitNull);
            when(permitRepository.findById(permitTest.getId())).thenReturn(isPermit);
            this.permitRepository.deleteById(permitTest.getId());
            String response = permitService.deletePermitById(permitTest.getId());
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, permitTest.getId()));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testDeletePermitByIdBadRequestException() {
        try {
            Set<EPermission> permissions = new HashSet<>();
            Permit permitTest = new Permit(1, 1, permissions, 1);
            when(permitRepository.findById(permitTest.getId())).thenThrow(new RestClientException(OBJECT));
            this.permitRepository.deleteById(permitTest.getId());
            String response = permitService.deletePermitById(permitTest.getId());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }

    /**
     * Test of isEnumPresent method, of class PermitServiceImp.
     */
    @Test
    public void testIsEnumPresent() {
        String str = "WRITE";
        EPermission expResult = EPermission.valueOf(str);
        EPermission response = PermitServiceImp.isEnumPresent(str);
        Assert.assertEquals(expResult, response);
    }
    
    @Test
    public void testIsEnumPresentNull() {
        String str = "";
        EPermission response = PermitServiceImp.isEnumPresent(str);
        Assert.assertNull(response);
    }
   

}
