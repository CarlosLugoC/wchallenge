/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.AddressDTO;
import ar.com.wolox.demo.dto.CommentDTO;
import ar.com.wolox.demo.dto.CompanyDTO;
import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class CommentServiceImpTest {
    
    private static final String URL = "https://jsonplaceholder.typicode.com/comments";
    private static final String URLUSER = "https://jsonplaceholder.typicode.com/users";
    private static final String OBJECT = "comment";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String STRINGFORMATEMAIL = "%s?email=%s";
    private static final String STRINGFORMATNAME = "%s?name=%s";
    
    @Mock
    private RestTemplate restTemplate;
    
    @Mock
    private MessagesInternationalizationImp msg;
    
    @InjectMocks
    private CommentServiceImp commentService;
    
    @Before
    public void setUp() {
    }

    /**
     * Test of getAllComments method, of class CommentServiceImp.
     */
    @Test
    public void testGetAllComments() {
        List<CommentDTO> comments = new ArrayList<>();
        CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
        comments.add(commentTest);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                .thenReturn(new ResponseEntity<>(comments, HttpStatus.OK));
        List<CommentDTO> response = commentService.getAllComments();
        Assert.assertNotNull(response);
        Assert.assertEquals(response, comments);
    }
    
    @Test(expected = BadRequestException.class)
    public void testGetAllCommentsBadReuqestException() {
        try {
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            List<CommentDTO> response = commentService.getAllComments();
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of getCommentById method, of class CommentServiceImp.
     */
    @Test
    public void testGetCommentById() {
        CommentDTO commentTest = new CommentDTO();
        commentTest.setId(1);
        commentTest.setPostId(1);
        String url = String.format(STRINGFORMATSLASH,URL, commentTest.getId());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<CommentDTO>() {}))
                .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
        CommentDTO response = commentService.getCommentById(commentTest.getId());
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getId(), commentTest.getId());
    }
    
    @Test(expected = NotFoundException.class)
    public void testGetCommentByIdNotFoundException() {
        CommentDTO commentTest = new CommentDTO();
        try {
            commentTest.setId(1);
            String url = String.format(STRINGFORMATSLASH,URL, commentTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            CommentDTO response = commentService.getCommentById(commentTest.getId());
        } catch(RestClientException ex){
            throw new NotFoundException(msg.msgNotFound(OBJECT, commentTest.getId()));
        }
        
    }

    /**
     * Test of getCommentByName method, of class CommentServiceImp.
     */
    @Test
    public void testGetCommentByName() {
        List<CommentDTO> commentListDTO = new ArrayList<>();
        CommentDTO commentTest = new CommentDTO();
        commentTest.setId(1);
        commentTest.setName("name");
        commentListDTO.add(commentTest);
        String url = String.format(STRINGFORMATNAME,URL, commentTest.getName());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                .thenReturn(new ResponseEntity<>(commentListDTO, HttpStatus.OK));
        List<CommentDTO> response = commentService.getCommentByName(commentTest.getName());
        Assert.assertNotNull(response);
        Assert.assertEquals(response.get(0).getId(), commentTest.getId());
    }
    
    @Test(expected = BadRequestException.class)
    public void testGetCommentByNameNotFoundException() {
        try {
            CommentDTO commentTest = new CommentDTO();
            commentTest.setId(1);
            commentTest.setName("name");
            String url = String.format(STRINGFORMATNAME,URL, commentTest.getName());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            List<CommentDTO> response = commentService.getCommentByName(commentTest.getName());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of getCommentByNameV2 method, of class CommentServiceImp.
     */
    @Test
    public void testGetCommentByNameV2() {
        List<CommentDTO> commentListDTO = new ArrayList<>();
        CommentDTO commentTest = new CommentDTO();
        commentTest.setId(1);
        commentTest.setName("name");
        commentListDTO.add(commentTest);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                .thenReturn(new ResponseEntity<>(commentListDTO, HttpStatus.OK));
        List<CommentDTO> response = commentService.getCommentByNameV2(commentTest.getName());
        Assert.assertNotNull(response);
        Assert.assertEquals(response.get(0).getId(), commentTest.getId());
    }
    /**
     * Test of getCommentsByEmail method, of class CommentServiceImp.
     */
    @Test
    public void testGetCommentsByEmail() {
        CommentDTO commentTest = new CommentDTO();
        commentTest.setId(1);
        commentTest.setEmail("email");
        List<CommentDTO> comments = new ArrayList<>();
        comments.add(commentTest);
        String url = String.format(STRINGFORMATEMAIL,URL, commentTest.getEmail());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                .thenReturn(new ResponseEntity<>(comments, HttpStatus.OK));
        List<CommentDTO> response = commentService.getCommentsByEmail(commentTest.getEmail());
        Assert.assertNotNull(response);
        Assert.assertEquals(response.get(0).getId(), comments.get(0).getId());
    }
    
    @Test(expected = BadRequestException.class)
    public void testGetCommentsByEmailBadRequestException() {
        try {
        CommentDTO commentTest = new CommentDTO();
        commentTest.setId(1);
        commentTest.setEmail("email");
        String url = String.format(STRINGFORMATEMAIL,URL, commentTest.getEmail());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                .thenThrow(new RestClientException(OBJECT));
        List<CommentDTO> response = commentService.getCommentsByEmail(commentTest.getEmail());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of getCommentsByUserId method, of class CommentServiceImp.
     */
    @Test
    public void testGetCommentsByUserId() {
        CommentDTO commentTest = new CommentDTO();
        commentTest.setId(1);
        commentTest.setEmail("email");
        List<CommentDTO> comments = new ArrayList<>();
        comments.add(commentTest);
        String url = String.format(STRINGFORMATEMAIL,URL, commentTest.getEmail());
        AddressDTO address = new AddressDTO();
        CompanyDTO company = new CompanyDTO();
        UserDTO userTest = new UserDTO(1, "prueba", "email", address, "154454", "www", company);
        String urlUser = String.format(STRINGFORMATSLASH,URLUSER, userTest.getId());
        when(restTemplate.exchange(
                urlUser, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                .thenReturn(new ResponseEntity<>(userTest, HttpStatus.OK));
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                .thenReturn(new ResponseEntity<>(comments, HttpStatus.OK));
        List<CommentDTO> response = commentService.getCommentsByUserId(userTest.getId());
        Assert.assertNotNull(response);
        Assert.assertEquals(response, comments);
    }
    
    @Test(expected = BadRequestException.class)
    public void testGetCommentsByUserIdBadRequestException() {
        try {
            CommentDTO commentTest = new CommentDTO();
            commentTest.setId(1);
            commentTest.setEmail("email");
            List<CommentDTO> comments = new ArrayList<>();
            comments.add(commentTest);
            String url = String.format(STRINGFORMATEMAIL,URL, commentTest.getEmail());
            AddressDTO address = new AddressDTO();
            CompanyDTO company = new CompanyDTO();
            UserDTO userTest = new UserDTO(1, "prueba", "email", address, "154454", "www", company);
            String urlUser = String.format(STRINGFORMATSLASH,URLUSER, userTest.getId());
            when(restTemplate.exchange(
                    urlUser, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(comments, HttpStatus.OK));
            List<CommentDTO> response = commentService.getCommentsByUserId(userTest.getId());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of createComment method, of class CommentServiceImp.
     */
    @Test
    public void testCreateComment() {
        CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
        List<CommentDTO> comments = new ArrayList<>();
        HttpEntity<CommentDTO> request = new HttpEntity<>(commentTest);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                .thenReturn(new ResponseEntity<>(comments, HttpStatus.OK));
        when(restTemplate.exchange(
                URL, HttpMethod.POST, request, new ParameterizedTypeReference<CommentDTO>() {}))
                .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
        CommentDTO response = commentService.createComment(commentTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getBody(), commentTest.getBody());
    }
    
    @Test(expected = ConflictException.class)
    public void testCreateCommentCreatedException() {
        try {
            CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
            List<CommentDTO> comments = new ArrayList<>();
            comments.add(commentTest);
            HttpEntity<CommentDTO> request = new HttpEntity<>(commentTest);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(comments, HttpStatus.OK));
            when(restTemplate.exchange(
                    URL, HttpMethod.POST, request, new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
            CommentDTO response = commentService.createComment(commentTest);
        } catch(RestClientException ex) {
            throw new ConflictException(msg.msgConflict(OBJECT));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testCreateCommentBadRequestException() {
        try {
            CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
            List<CommentDTO> comments = new ArrayList<>();
            HttpEntity<CommentDTO> request = new HttpEntity<>(commentTest);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<CommentDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(comments, HttpStatus.OK));
            when(restTemplate.exchange(
                    URL, HttpMethod.POST, request, new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            CommentDTO response = commentService.createComment(commentTest);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }

    /**
     * Test of modifyComment method, of class CommentServiceImp.
     */
    @Test
    public void testUpdateComment() {
        CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
        String url = String.format(STRINGFORMATSLASH,URL, commentTest.getId());
        HttpEntity<CommentDTO> request = new HttpEntity<>(commentTest);
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<CommentDTO>() {}))
                .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
        when(restTemplate.exchange(
                url, HttpMethod.PUT, request, new ParameterizedTypeReference<CommentDTO>() {}))
                .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
        CommentDTO response = commentService.updateComment(commentTest.getId(),commentTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response, commentTest);
    }
    
    @Test(expected = NotFoundException.class)
    public void testUpdateCommentNotFoundException() {
        CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
        try {
            CommentDTO commentNull = null;
            String url = String.format(STRINGFORMATSLASH,URL, commentTest.getId());
            HttpEntity<CommentDTO> request = new HttpEntity<>(commentTest);
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenReturn(new ResponseEntity<>(commentNull, HttpStatus.OK));
            when(restTemplate.exchange(
                    url, HttpMethod.PUT, request, new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
            CommentDTO response = commentService.updateComment(commentTest.getId(),commentTest);
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, commentTest.getId()));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testUpdateCommentBadRequestException() {
        try {
            CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
            String url = String.format(STRINGFORMATSLASH,URL, commentTest.getId());
            HttpEntity<CommentDTO> request = new HttpEntity<>(commentTest);
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
            when(restTemplate.exchange(
                    url, HttpMethod.PUT, request, new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            CommentDTO response = commentService.updateComment(commentTest.getId(),commentTest);
        } catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
    
    }

    /**
     * Test of deleteCommentById method, of class CommentServiceImp.
     */
    @Test
    public void testDeleteCommentById() {
        CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
        String msgR = "OK";
        String url = String.format(STRINGFORMATSLASH,URL, commentTest.getId());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<CommentDTO>() {}))
                .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
        this.restTemplate.delete(url);
        when(msg.msgSuccesfullyDelete(OBJECT, commentTest.getId())).thenReturn(msgR);
        String response = commentService.deleteCommentById(commentTest.getId());
        Assert.assertNotNull(response);
    }
    
    @Test(expected = NotFoundException.class)
    public void testDeleteCommentByIdNotFoundException() {
        CommentDTO commentTest = new CommentDTO(1, "name", "email", "body", 1);
        try {
            CommentDTO commentNull = null;
            String url = String.format(STRINGFORMATSLASH,URL, commentTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenReturn(new ResponseEntity<>(commentNull, HttpStatus.OK));
            this.restTemplate.delete(url);
            String response = commentService.deleteCommentById(commentTest.getId());
        } catch(RestClientException ex){
            throw new NotFoundException(msg.msgNotFound(OBJECT, commentTest.getId()));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testDeleteCommentByIdBadRequestException() {
        try {
            CommentDTO commentTest = new CommentDTO();
            String url = String.format(STRINGFORMATSLASH,URL, commentTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<CommentDTO>() {}))
                    .thenReturn(new ResponseEntity<>(commentTest, HttpStatus.OK));
            this.restTemplate.delete(url);
            when(commentService.deleteCommentById(commentTest.getId())).
                    thenThrow(new RestClientException(OBJECT));
            String response = commentService.deleteCommentById(commentTest.getId());
        } catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
}
