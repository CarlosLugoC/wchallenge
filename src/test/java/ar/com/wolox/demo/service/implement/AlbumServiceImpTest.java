/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.AlbumDTO;
import ar.com.wolox.demo.dto.PermitDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import ar.com.wolox.demo.model.Permit;
import ar.com.wolox.demo.repository.PermitRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class AlbumServiceImpTest {
    
    private static final String URL = "https://jsonplaceholder.typicode.com/albums";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String STRINGFORMATUSERID = "%s?userId=%s";
    private static final String OBJECT = "album";
    
    @Mock
    private RestTemplate restTemplate;
    
    @Mock
    private PermitServiceImp permitService;
    
    @Mock
    private PermitRepository permitRespository;
    
    @Mock
    private MessagesInternationalizationImp msg;
    
    @InjectMocks
    private AlbumServiceImp albumService;
    
    @Before
    public void setUp() {
    }

    /**
     * Test of getAllAlbums method, of class AlbumServiceImp.
     */
    @Test
    public void testGetAllAlbums() {
        List<AlbumDTO> albums = new ArrayList<>();
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        albums.add(albumTest);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                .thenReturn(new ResponseEntity<>(albums, HttpStatus.OK));
        List<AlbumDTO> response = albumService.getAllAlbums();
        Assert.assertNotNull(response);
        Assert.assertEquals(response, albums);
    }
    
    @Test(expected = BadRequestException.class)
    public void testGetAllAlbumsBadRequestException() {
        try {
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            List<AlbumDTO> response = albumService.getAllAlbums();
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }
    /**
     * Test of getAlbumsByUser method, of class AlbumServiceImp.
     */
    @Test
    public void testGetAlbumsByUser() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        List<AlbumDTO> albums = new ArrayList<>();
        List<PermitDTO> permits = new ArrayList<>();
        Page<PermitDTO> permitsPage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 1;
        List<Permit> listPermit = new ArrayList<>();
        listPermit.add(new Permit());
        String url = String.format(STRINGFORMATUSERID, URL, albumTest.getUserId());
        when(permitRespository.findAll()).thenReturn(listPermit);
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                .thenReturn(new ResponseEntity<>(albums, HttpStatus.OK));
        when(permitService.getPermitsByUser(albumTest.getUserId(),pageNumber,pageSize)).thenReturn(permitsPage);
        List<AlbumDTO> response = albumService.getAlbumsByUser(albumTest.getUserId());
        Assert.assertNotNull(response);
        Assert.assertEquals(response, albums);
    }
    
    @Test
    public void testGetAlbumsByUserPermits() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        List<AlbumDTO> albums = new ArrayList<>();
        albums.add(albumTest);
        List<PermitDTO> permits = new ArrayList<>();
        Set<String> permissions = new HashSet<>();
        permissions.add("READ");
        PermitDTO permitTest = new PermitDTO(1, 1, permissions, 1);
        permits.add(permitTest);
        Page<PermitDTO> permitsPage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 1;
        List<Permit> listPermit = new ArrayList<>();
        listPermit.add(new Permit());
        String url = String.format(STRINGFORMATUSERID, URL, albumTest.getUserId());
        String url2 = String.format(STRINGFORMATSLASH,URL,albumTest.getId());
        when(permitRespository.findAll()).thenReturn(listPermit);
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                .thenReturn(new ResponseEntity<>(albums, HttpStatus.OK));
        when(permitService.getPermitsByUser(albumTest.getUserId(),pageNumber,pageSize)).thenReturn(permitsPage);
        when(restTemplate.exchange(url2, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<AlbumDTO>() {}))
                .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
        List<AlbumDTO> response = albumService.getAlbumsByUser(albumTest.getUserId());
        Assert.assertNotNull(response);
        Assert.assertEquals(response, albums);
    }
    
    @Test
    public void testGetAlbumsByUserPermitsAddAlbum() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        AlbumDTO albumTestAdd = new AlbumDTO(2, "title", 2);
        List<AlbumDTO> albums = new ArrayList<>();
        albums.add(albumTest);
        List<PermitDTO> permits = new ArrayList<>();
        Set<String> permissions = new HashSet<>();
        permissions.add("READ");
        PermitDTO permitTest = new PermitDTO(1, 1, permissions, 1);
        permits.add(permitTest);
        Page<PermitDTO> permitsPage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 1;
        List<Permit> listPermit = new ArrayList<>();
        listPermit.add(new Permit());
        String url = String.format(STRINGFORMATUSERID, URL, albumTest.getUserId());
        String url2 = String.format(STRINGFORMATSLASH,URL,albumTest.getId());
        when(permitRespository.findAll()).thenReturn(listPermit);
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                .thenReturn(new ResponseEntity<>(albums, HttpStatus.OK));
        when(permitService.getPermitsByUser(albumTest.getUserId(),pageNumber,pageSize)).thenReturn(permitsPage);
        when(restTemplate.exchange(url2, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<AlbumDTO>() {}))
                .thenReturn(new ResponseEntity<>(albumTestAdd, HttpStatus.OK));
        List<AlbumDTO> response = albumService.getAlbumsByUser(albumTest.getUserId());
        albums.add(albumTestAdd);
        Assert.assertNotNull(response);
        Assert.assertEquals(response, albums);
    }
    
    @Test(expected = BadRequestException.class)
    public void testGetAlbumsByUserBadRequestException() {
        try {
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            String url = String.format(STRINGFORMATUSERID, URL, albumTest.getUserId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                    .thenThrow(new RestClientException(url));
            List<AlbumDTO> response = albumService.getAlbumsByUser(albumTest.getUserId());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }
    /**
     * Test of getAlbumById method, of class AlbumServiceImp.
     */
    @Test
    public void testGetAlbumById() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        String url = String.format(STRINGFORMATSLASH,URL,albumTest.getId());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<AlbumDTO>() {}))
                .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
        AlbumDTO response = albumService.getAlbumById(albumTest.getId());
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getTitle(), albumTest.getTitle());
    }
    
    /**
     * Test of getAlbumById method, of class AlbumServiceImp.
     */
    @Test(expected = NotFoundException.class)
    public void testGetAlbumByIdException() {
        AlbumDTO albumTest = new AlbumDTO();
        try {
            String url = String.format(STRINGFORMATSLASH,URL,albumTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenThrow(new RestClientException(url));
            AlbumDTO response = albumService.getAlbumById(albumTest.getId());
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT,albumTest.getId()));
        }
    }

    /**
     * Test of createAlbum method, of class AlbumServiceImp.
     */
    @Test
    public void testCreateAlbum() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        HttpEntity<AlbumDTO> request = new HttpEntity<>(albumTest);
        List<AlbumDTO> albums = new ArrayList<>();
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                .thenReturn(new ResponseEntity<>(albums, HttpStatus.OK));
        when(restTemplate.exchange(
                URL, HttpMethod.POST, request, new ParameterizedTypeReference<AlbumDTO>() {}))
                .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
        AlbumDTO response = albumService.createAlbum(albumTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getUserId(), albumTest.getUserId());
    }
    
    @Test(expected = BadRequestException.class)
    public void testCreateAlbumBadRequestException() {
        try {
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            HttpEntity<AlbumDTO> request = new HttpEntity<>(albumTest);
            List<AlbumDTO> albums = new ArrayList<>();
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(albums, HttpStatus.OK));
            when(restTemplate.exchange(
                    URL, HttpMethod.POST, request, new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            AlbumDTO response = albumService.createAlbum(albumTest);
        }catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }
    
    @Test(expected = ConflictException.class)
    public void testCreateCreatedException() {
        try {
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            HttpEntity<AlbumDTO> request = new HttpEntity<>(albumTest);
            List<AlbumDTO> albums = new ArrayList<>();
            albums.add(albumTest);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(albums, HttpStatus.OK));
            when(restTemplate.exchange(
                    URL, HttpMethod.POST, request, new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
            AlbumDTO response = albumService.createAlbum(albumTest);
        } catch(RestClientException ex){
            throw new ConflictException(msg.msgConflict(OBJECT));
        }
    }

    /**
     * Test of modifyAlbum method, of class AlbumServiceImp.
     */
    @Test
    public void testUpdateAlbum() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        String url = String.format(STRINGFORMATSLASH,URL, albumTest.getId());
        HttpEntity<AlbumDTO> request = new HttpEntity<>(albumTest);
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<AlbumDTO>() {}))
                .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
        when(restTemplate.exchange(
                url, HttpMethod.PUT, request, new ParameterizedTypeReference<AlbumDTO>() {}))
                .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
        AlbumDTO response = albumService.updateAlbum(albumTest.getId(),albumTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getId(), albumTest.getId());
    }
    
    @Test(expected = BadRequestException.class)
    public void testUpdateAlbumBadRequestException() {
        try {
            AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
            String url = String.format(STRINGFORMATSLASH,URL, albumTest.getId());
            HttpEntity<AlbumDTO> request = new HttpEntity<>(albumTest);
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
            when(restTemplate.exchange(
                    url, HttpMethod.PUT, request, new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenThrow(new RestClientException(url));
            AlbumDTO response = albumService.updateAlbum(albumTest.getId(),albumTest);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
        
    }
    
    @Test(expected = NotFoundException.class)
    public void testUpdateAlbumNotFoundException() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        try {
            
            AlbumDTO albumNull = null;
            String url = String.format(STRINGFORMATSLASH,URL, albumTest.getId());
            HttpEntity<AlbumDTO> request = new HttpEntity<>(albumTest);
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenReturn(new ResponseEntity<>(albumNull, HttpStatus.OK));
            when(restTemplate.exchange(
                    url, HttpMethod.PUT, request, new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
            AlbumDTO response = albumService.updateAlbum(albumTest.getId(),albumTest);
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, albumTest.getId()));
        }
        
    }

    /**
     * Test of deleteAlbumById method, of class AlbumServiceImp.
     */
    @Test
    public void testDeleteAlbumById() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        String msgR = "OK";
        String url = String.format(STRINGFORMATSLASH,URL, albumTest.getId());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<AlbumDTO>() {}))
                .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
        this.restTemplate.delete(url);
        when(msg.msgSuccesfullyDelete(OBJECT, albumTest.getId())).thenReturn(msgR);
        String response = albumService.deleteAlbumById(albumTest.getId());
        Assert.assertNotNull(response);
    }
    
    @Test(expected = BadRequestException.class)
    public void testDeleteAlbumByIdBadRequestException() {
        try {
            AlbumDTO albumTest = new AlbumDTO();
            String url = String.format(STRINGFORMATSLASH,URL, albumTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenReturn(new ResponseEntity<>(albumTest, HttpStatus.OK));
            this.restTemplate.delete(url);
            when(albumService.deleteAlbumById(albumTest.getId())).
                    thenThrow(new RestClientException(url));
            String response = albumService.deleteAlbumById(albumTest.getId());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
    @Test(expected = NotFoundException.class)
    public void testDeleteAlbumByIdNotFoundException() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        try {
            
            AlbumDTO albumNull = null;
            String url = String.format(STRINGFORMATSLASH,URL, albumTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<AlbumDTO>() {}))
                    .thenReturn(new ResponseEntity<>(albumNull, HttpStatus.OK));
            restTemplate.delete(url);
            String response = albumService.deleteAlbumById(albumTest.getId());
       } catch(RestClientException ex) {
           throw new NotFoundException(msg.msgNotFound(OBJECT, albumTest.getId()));
       }
    }
}
