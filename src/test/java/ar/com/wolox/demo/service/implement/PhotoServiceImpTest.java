/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.AlbumDTO;
import ar.com.wolox.demo.dto.PhotoDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class PhotoServiceImpTest {
    
    private static final String URL = "https://jsonplaceholder.typicode.com/photos";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String STRINGFORMATALBUM = "%s?albumId=%s";
    private static final String OBJECT = "photo";
    
    @Mock
    private RestTemplate restTemplate;
   
    @Mock
    private AlbumServiceImp albumService;
    
    @Mock
    private MessagesInternationalizationImp msg;
    
    @InjectMocks
    private PhotoServiceImp photoService;
    
    public PhotoServiceImpTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of getAllPhotos method, of class PhotoServiceImp.
     */
    @Test
    public void testGetAllPhotos() {
        List<PhotoDTO> photos = new ArrayList<>();
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnailUrl", 1);
        photos.add(photoTest);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                .thenReturn(new ResponseEntity<>(photos, HttpStatus.OK));
        List<PhotoDTO> response = photoService.getAllPhotos();
        Assert.assertNotNull(response);
        Assert.assertEquals(response, photos);
    }

    @Test(expected = BadRequestException.class)
    public void testGetAllPhotosBadRequestException() {
        try {
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            List<PhotoDTO> response = photoService.getAllPhotos();
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }
    /**
     * Test of getPhotosByAlbumId method, of class PhotoServiceImp.
     */
    @Test
    public void testGetPhotosByAlbumId() {
        List<PhotoDTO> photos = new ArrayList<>();
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
        photos.add(photoTest);
        String url = String.format(STRINGFORMATALBUM,URL,photoTest.getAlbumId());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                .thenReturn(new ResponseEntity<>(photos, HttpStatus.OK));
        List<PhotoDTO> response = photoService.getPhotosByAlbumId(photoTest.getAlbumId());
        Assert.assertNotNull(response);
        Assert.assertEquals(response.get(0).getThumbnailUrl(), photos.get(0).getThumbnailUrl());
    }

    @Test(expected = BadRequestException.class)
    public void testGetPhotosByAlbumIdBadRequestException() {
        try{
            List<PhotoDTO> photos = new ArrayList<>();
            PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
            photos.add(photoTest);
            String url = String.format(STRINGFORMATALBUM,URL,photoTest.getAlbumId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            List<PhotoDTO> response = photoService.getPhotosByAlbumId(photoTest.getAlbumId());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }
    /**
     * Test of getPhotosByUser method, of class PhotoServiceImp.
     */
    @Test
    public void testGetPhotosByUser() {
        List<PhotoDTO> photos = new ArrayList<>();
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
        List<AlbumDTO> albums = new ArrayList<>();
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        photos.add(photoTest);
        albums.add(albumTest);
        when(albumService.getAlbumsByUser(albumTest.getUserId())).thenReturn(albums);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                .thenReturn(new ResponseEntity<>(photos, HttpStatus.OK));
        List<PhotoDTO> response = photoService.getPhotosByUser(albumTest.getUserId());
        Assert.assertNotNull(response);
        Assert.assertEquals(response, photos);
    }
    
    @Test(expected = NotFoundException.class)
    public void testGetPhotosByUserBadRequestException() {
        AlbumDTO albumTest = new AlbumDTO(1, "title", 1);
        try {
            List<PhotoDTO> photos = new ArrayList<>();
            PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 2);
            photos.add(photoTest);
            List<AlbumDTO> albums = new ArrayList<>();
            when(albumService.getAlbumsByUser(albumTest.getUserId())).thenReturn(albums);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(photos, HttpStatus.OK));
            List<PhotoDTO> response = photoService.getPhotosByUser(albumTest.getUserId());
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, albumTest.getUserId()));
        }
    }

    /**
     * Test of getPhotoById method, of class PhotoServiceImp.
     */
    @Test
    public void testGetPhotoById() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
        String url = String.format(STRINGFORMATSLASH,URL,photoTest.getId());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<PhotoDTO>() {}))
                .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
        PhotoDTO response = photoService.getPhotoById(photoTest.getId());
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getUrl(), photoTest.getUrl());
    }
    
    @Test(expected = NotFoundException.class)
    public void testGetPhotoByIdNotFoundException() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
        try {
            String url = String.format(STRINGFORMATSLASH,URL,photoTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            PhotoDTO response = photoService.getPhotoById(photoTest.getId());
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, photoTest.getId()));
        }
    }

    /**
     * Test of createPhoto method, of class PhotoServiceImp.
     */
    @Test
    public void testCreatePhoto() {
        PhotoDTO photoTest = new PhotoDTO();
        photoTest.setId(1);
        photoTest.setTitle("title");
        photoTest.setUrl("url");
        photoTest.setAlbumId(1);
        photoTest.setThumbnailUrl("thumbnail");
        List<PhotoDTO> photos = new ArrayList<>();
        HttpEntity<PhotoDTO> request = new HttpEntity<>(photoTest);
        when(restTemplate.exchange(
                URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                .thenReturn(new ResponseEntity<>(photos, HttpStatus.OK));
        when(restTemplate.exchange(
                URL, HttpMethod.POST, request,new ParameterizedTypeReference<PhotoDTO>() {}))
                .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
        PhotoDTO response = photoService.createPhoto(photoTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getTitle(), photoTest.getTitle());
    }
    
    @Test(expected = ConflictException.class)
    public void testCreatePhotoNotFoundException() {
        try {
            PhotoDTO photoTest = new PhotoDTO();
            photoTest.setId(1);
            photoTest.setTitle("title");
            photoTest.setUrl("url");
            photoTest.setAlbumId(1);
            photoTest.setThumbnailUrl("thumbnail");
            List<PhotoDTO> photos = new ArrayList<>();
            photos.add(photoTest);
            HttpEntity<PhotoDTO> request = new HttpEntity<>(photoTest);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(photos, HttpStatus.OK));
            when(restTemplate.exchange(
                    URL, HttpMethod.POST, request,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
            PhotoDTO response = photoService.createPhoto(photoTest);
        } catch(RestClientException ex) {
            throw new ConflictException(msg.msgConflict(OBJECT));
        }
    }
    
    @Test(expected = BadRequestException.class)
    public void testCreatePhotoBadRequestException() {
        try {
            PhotoDTO photoTest = new PhotoDTO();
            photoTest.setId(1);
            photoTest.setTitle("title");
            photoTest.setUrl("url");
            photoTest.setAlbumId(1);
            photoTest.setThumbnailUrl("thumbnail");
            List<PhotoDTO> photos = new ArrayList<>();
            HttpEntity<PhotoDTO> request = new HttpEntity<>(photoTest);
            when(restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<PhotoDTO>>() {}))
                    .thenReturn(new ResponseEntity<>(photos, HttpStatus.OK));
            when(restTemplate.exchange(
                    URL, HttpMethod.POST, request,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            PhotoDTO response = photoService.createPhoto(photoTest);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }

    /**
     * Test of modifyPhoto method, of class PhotoServiceImp.
     */
    @Test
    public void testUpdatePhoto() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
        HttpEntity<PhotoDTO> request = new HttpEntity<>(photoTest);
        String url = String.format(STRINGFORMATSLASH,URL,photoTest.getId());
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<PhotoDTO>() {}))
                .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
        when(restTemplate.exchange(
                url, HttpMethod.PUT, request,new ParameterizedTypeReference<PhotoDTO>() {}))
                .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
        PhotoDTO response = photoService.updatePhoto(photoTest.getId(),photoTest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getAlbumId(), photoTest.getAlbumId());
    }
    
    @Test(expected = NotFoundException.class)
    public void testUpdatePhotoNotFoundException() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
        try {
            PhotoDTO photoNull = null;
            HttpEntity<PhotoDTO> request = new HttpEntity<>(photoTest);
            String url = String.format(STRINGFORMATSLASH,URL,photoTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenReturn(new ResponseEntity<>(photoNull, HttpStatus.OK));
            when(restTemplate.exchange(
                    url, HttpMethod.PUT, request,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
            PhotoDTO response = photoService.updatePhoto(photoTest.getId(),photoTest);
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, photoTest.getId()));
        } 
    }
    
    @Test(expected = BadRequestException.class)
    public void testUpdatePhotoBadRequestException() {
        try {
            PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
            HttpEntity<PhotoDTO> request = new HttpEntity<>(photoTest);
            String url = String.format(STRINGFORMATSLASH,URL,photoTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
            when(restTemplate.exchange(
                    url, HttpMethod.PUT, request,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            PhotoDTO response = photoService.updatePhoto(photoTest.getId(),photoTest);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        } 
    }

    /**
     * Test of deletePhotoById method, of class PhotoServiceImp.
     */
    @Test
    public void testDeletePhotoById() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
        String url = String.format(STRINGFORMATSLASH,URL,photoTest.getId());
        String msgR = "OK";
        when(restTemplate.exchange(
                url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<PhotoDTO>() {}))
                .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
        this.restTemplate.delete(url);
        when(msg.msgSuccesfullyDelete(OBJECT, photoTest.getId())).thenReturn(msgR);
        String response = photoService.deletePhotoById(photoTest.getId());
        Assert.assertNotNull(response);
    }
    
    @Test(expected = BadRequestException.class)
    public void testDeletePhotoBadRequestExceptionById() {
        try {
            PhotoDTO photoTest = new PhotoDTO();
            String url = String.format(STRINGFORMATSLASH,URL,photoTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenReturn(new ResponseEntity<>(photoTest, HttpStatus.OK));
            this.restTemplate.delete(url);
            when(photoService.deletePhotoById(photoTest.getId())).
                    thenThrow(new RestClientException(OBJECT));
            String response = photoService.deletePhotoById(photoTest.getId());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
    @Test(expected = NotFoundException.class)
    public void testDeletePhotoNotFoundExceptionById() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnail", 1);
        try {
            PhotoDTO photoNull = null;
            String url = String.format(STRINGFORMATSLASH,URL,photoTest.getId());
            when(restTemplate.exchange(
                    url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<PhotoDTO>() {}))
                    .thenReturn(new ResponseEntity<>(photoNull, HttpStatus.OK));
            this.restTemplate.delete(url);
            String response = photoService.deletePhotoById(photoTest.getId());
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, photoTest.getId()));
        }
    }
    

    
}
