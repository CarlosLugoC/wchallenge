/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.PostDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class PostServicesImpTest {
    
    private static final String URL = "https://jsonplaceholder.typicode.com/posts";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String OBJECT = "post";
    
    @Mock
    private RestTemplate restTemplate;
    
    @Mock
    private MessagesInternationalizationImp msg;
    
    @InjectMocks
    private PostServicesImp postServices;
    
    @Before
    public void setUp() {
    }

    /**
     * Test of getAllPosts method, of class PostServicesImp.
     */
    @Test
    public void testGetAllPosts() {
        List<PostDTO> postListDTO = new ArrayList<>();
        PostDTO postTest = new PostDTO();
        postTest.setId(1);
        postTest.setBody("body");
        postTest.setTitle("title");
        postTest.setUserId(1);
        postListDTO.add(postTest);
        when(restTemplate.exchange(URL, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<PostDTO>>() {}))
                .thenReturn(new ResponseEntity<>(postListDTO, HttpStatus.OK));
        List<PostDTO> response = postServices.getAllPosts();
        assertEquals(response, postListDTO);
    }
    
    @Test(expected = BadRequestException.class)
    public void testGetAllPostsBadRequestException() {
        try {
            when(restTemplate.exchange(URL, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<PostDTO>>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            List<PostDTO> response = postServices.getAllPosts();
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * Test of getPostById method, of class PostServicesImp.
     */
    @Test
    public void testGetPostById() {
        PostDTO postTest = new PostDTO(1, 1, "title", "body");
        String url = String.format(STRINGFORMATSLASH,URL, postTest.getId());
        when(restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<PostDTO>() {}))
                .thenReturn(new ResponseEntity<>(postTest, HttpStatus.OK));
        PostDTO response = postServices.getPostById(postTest.getId());
        assertEquals(response.getBody(), postTest.getBody());
        assertEquals(response.getTitle(), postTest.getTitle());
        assertEquals(response.getUserId(), postTest.getUserId());
    }
    
    @Test(expected = NotFoundException.class)
    public void testGetPostByIdNotFoundException() {
        PostDTO postTest = new PostDTO(1, 1, "title", "body");
        try {
            String url = String.format(STRINGFORMATSLASH,URL, postTest.getId());
            when(restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<PostDTO>() {}))
                    .thenThrow(new RestClientException(OBJECT));
            PostDTO response = postServices.getPostById(postTest.getId());
        } catch(RestClientException ex){
            throw new NotFoundException(msg.msgNotFound(OBJECT, postTest.getId()));
        }
    }
    
}
