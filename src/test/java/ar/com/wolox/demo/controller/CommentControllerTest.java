/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.CommentDTO;
import ar.com.wolox.demo.service.implement.CommentServiceImp;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class CommentControllerTest {

    @Mock
    private CommentServiceImp commentService;
    
    @InjectMocks
    private CommentController commentController;
    
    @Before
    public void setUp() {
    }

    /**
     * Test of getAllComments method, of class CommentController.
     */
    @Test
    public void testGetAllComments() {
        CommentDTO commentDTOTest = new CommentDTO(1, "name", "email", "body", 1);
        List<CommentDTO> comments = new ArrayList<>();
        comments.add(commentDTOTest);
        when(commentService.getAllComments()).thenReturn(comments);
        ResponseEntity<List<CommentDTO>> response = commentController.getAllComments();
        assertNotNull(response);
        assertEquals(response.getBody(), comments);
        
    }
    
    @Test
    public void testGetAllCommentsEmpty() {
        List<CommentDTO> comments = new ArrayList<>();
        when(commentService.getAllComments()).thenReturn(comments);
        ResponseEntity<List<CommentDTO>> response = commentController.getAllComments();
        assertNotNull(response);
        assertNull(response.getBody());
    }
    
     /**
     * Test of getCommentByIdmethod, of class CommentController.
     */
    @Test
    public void testGetCommentById() {
        CommentDTO commentDTOTest = new CommentDTO();
        commentDTOTest.setId(1);
        when(commentService.getCommentById(commentDTOTest.getId())).thenReturn(commentDTOTest);
        ResponseEntity<CommentDTO> response = commentController.getCommentById(commentDTOTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody(), commentDTOTest);
    }

    /**
     * Test of getCommentByName method, of class CommentController.
     */
    @Test
    public void testGetCommentByName() {
        List<CommentDTO> commentListDTO = new ArrayList<>();
        CommentDTO commentDTOTest = new CommentDTO();
        commentDTOTest.setName("name");
        commentListDTO.add(commentDTOTest);
        when(commentService.getCommentByNameV2(commentDTOTest.getName())).thenReturn(commentListDTO);
        ResponseEntity<List<CommentDTO>> response = commentController.getCommentByName(commentDTOTest.getName());
        assertNotNull(response);
        assertEquals(response.getBody(), commentListDTO);
    }
    
    @Test
    public void testGetCommentByNameEmpty() {
        List<CommentDTO> commentListDTO = new ArrayList<>();
        CommentDTO commentDTOTest = new CommentDTO();
        when(commentService.getCommentByNameV2(commentDTOTest.getName())).thenReturn(commentListDTO);
        ResponseEntity<List<CommentDTO>> response = commentController.getCommentByName(commentDTOTest.getName());
        assertNotNull(response);
        assertNull(response.getBody());
    }

    /**
     * Test of getCommentsByUserId method, of class CommentController.
     */
    @Test
    public void testGetCommentsByUserId() {
        CommentDTO commentDTOTest = new CommentDTO();
        commentDTOTest.setEmail("email");
        List<CommentDTO> comments = new ArrayList<>();
        comments.add(commentDTOTest);
        when(commentService.getCommentsByUserId(1)).thenReturn(comments);
        ResponseEntity<List<CommentDTO>> response = commentController.getCommentsByUserId(1);
        assertNotNull(response);
        assertEquals(response.getBody(), comments);
    }
    @Test
    public void testGetCommentsByUserIdEmpty() {
        CommentDTO commentDTOTest = new CommentDTO();
        commentDTOTest.setEmail("email");
        List<CommentDTO> comments = new ArrayList<>();
        when(commentService.getCommentsByUserId(1)).thenReturn(comments);
        ResponseEntity<List<CommentDTO>> response = commentController.getCommentsByUserId(1);
        assertNotNull(response);
        assertNull(response.getBody());
    }
    

    /**
     * Test of getCommentsByEmail method, of class CommentController.
     */
    @Test
    public void testGetCommentsByEmail() {
        CommentDTO commentDTOTest = new CommentDTO();
        commentDTOTest.setEmail("email");
        List<CommentDTO> comments = new ArrayList<>();
        comments.add(commentDTOTest);
        when(commentService.getCommentsByEmail(commentDTOTest.getEmail())).thenReturn(comments);
        ResponseEntity<List<CommentDTO>> response = commentController.getCommentsByEmail(commentDTOTest.getEmail());
        assertNotNull(response);
        assertEquals(response.getBody(), comments);
    }
    
    @Test
    public void testGetCommentsByEmailEMpty() {
        CommentDTO commentDTOTest = new CommentDTO();
        commentDTOTest.setEmail("email");
        List<CommentDTO> comments = new ArrayList<>();
        when(commentService.getCommentsByEmail(commentDTOTest.getEmail())).thenReturn(comments);
        ResponseEntity<List<CommentDTO>> response = commentController.getCommentsByEmail(commentDTOTest.getEmail());
        assertNotNull(response);
        assertNull(response.getBody());
    }

    /**
     * Test of addComment method, of class CommentController.
     */
    @Test
    public void testAddComment() {
        CommentDTO commentDTOTest = new CommentDTO(1, "name", "email", "body", 1);
        when(commentService.createComment(commentDTOTest)).thenReturn(commentDTOTest);
        ResponseEntity<CommentDTO> response = commentController.addComment(commentDTOTest);
        assertNotNull(response);
        assertEquals(response.getBody().getBody(), commentDTOTest.getBody());
    }

    /**
     * Test of modifyComment method, of class CommentController.
     */
    @Test
    public void testUpdateComment() {
        CommentDTO commentDTOTest = new CommentDTO(1, "name", "email", "body", 1);
        when(commentService.updateComment(commentDTOTest.getId(),commentDTOTest)).thenReturn(commentDTOTest);
        ResponseEntity<CommentDTO> response = commentController.updateComment(commentDTOTest.getId(),commentDTOTest);
        assertNotNull(response);
        assertEquals(response.getBody().getPostId(), commentDTOTest.getPostId());
    }

    /**
     * Test of deleteCommentById method, of class CommentController.
     */
    @Test
    public void testDeleteCommentById() {
        CommentDTO commentDTOTest = new CommentDTO(1, "name", "email", "body", 1);
        String deleteMsg = "Delete";
        when(commentService.deleteCommentById(commentDTOTest.getId())).thenReturn(deleteMsg);
        ResponseEntity<String> response = commentController.deleteCommentById(commentDTOTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody(), deleteMsg);
    }
    
}
