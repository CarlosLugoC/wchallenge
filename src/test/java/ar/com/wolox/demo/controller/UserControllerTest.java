/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.AddressDTO;
import ar.com.wolox.demo.dto.CompanyDTO;
import ar.com.wolox.demo.dto.GeoDTO;
import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.service.implement.UserServiceImp;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class UserControllerTest {
    @Mock
    private UserServiceImp userService;
    
    @InjectMocks
    private UserController userController;

    @Before
    public void setUp() {
    }

    /**
     * Test of getAllUsers method, of class UserController.
     */
    @Test
    public void testGetAllUsers() {
        UserDTO userTest = new UserDTO();
        List<UserDTO> users = new ArrayList<>();
        users.add(userTest);
        when(userService.getAllUsers()).thenReturn(users);
        ResponseEntity<List<UserDTO>> response = userController.getAllUsers();
        assertNotNull(response);
        assertEquals(response.getBody(), users);

    }
    
        @Test
    public void testGetAllUsersEmpty() {
        List<UserDTO> users = new ArrayList<>();
        when(userService.getAllUsers()).thenReturn(users);
        ResponseEntity<List<UserDTO>> response = userController.getAllUsers();
        assertNotNull(response);

    }

    /**
     * Test of getUserById method, of class UserController.
     */
    @Test
    public void testGetUserById() {
        GeoDTO geo = new GeoDTO();
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO();
        company.setBs("bs");
        company.setCatchPhrase("catchPhrase");
        company.setName("name");
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        when(userService.getUserById(userTest.getId())).thenReturn(userTest);
        ResponseEntity<UserDTO> response = userController.getUserById(userTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody().getPhone(), userTest.getPhone());
        assertEquals(response.getBody().getCompany().getBs(), userTest.getCompany().getBs());
        assertEquals(response.getBody().getCompany().getCatchPhrase(), userTest.getCompany().getCatchPhrase());
        assertEquals(response.getBody().getCompany().getName(), userTest.getCompany().getName());
    }

    /**
     * Test of addUser method, of class UserController.
     */
    @Test
    public void testAddUser() {
        GeoDTO geo = new GeoDTO();
        AddressDTO address = new AddressDTO();
        address.setGeo(geo);
        address.setCity("Pereira");
        address.setStreet("street");
        address.setSuite("suite");
        address.setZipcode("zipcode");
        CompanyDTO company = new CompanyDTO();
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        when(userService.createUser(userTest)).thenReturn(userTest);
        ResponseEntity<UserDTO> response = userController.addUser(userTest);
        assertNotNull(response);
        assertEquals(response.getBody().getWebsite(), userTest.getWebsite());
        assertEquals(response.getBody().getAddress().getCity(), userTest.getAddress().getCity());
        assertEquals(response.getBody().getAddress().getStreet(), userTest.getAddress().getStreet());
        assertEquals(response.getBody().getAddress().getSuite(), userTest.getAddress().getSuite());
        assertEquals(response.getBody().getAddress().getZipcode(), userTest.getAddress().getZipcode());
    }

    /**
     * Test of modifyUser method, of class UserController.
     */
    @Test
    public void testUpdateUser() {
        GeoDTO geo = new GeoDTO();
        geo.setLat("lat");
        geo.setLng("lng");
        AddressDTO address = new AddressDTO("street", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO();
        userTest.setId(1);
        userTest.setAddress(address);
        userTest.setCompany(company);
        userTest.setEmail("email");
        userTest.setPhone("31245");
        userTest.setUsername("username");
        userTest.setWebsite("website");
        when(userService.updateUser(userTest.getId(),userTest)).thenReturn(userTest);
        ResponseEntity<UserDTO> response = userController.updateUser(userTest.getId(),userTest);
        assertNotNull(response);
        assertEquals(response.getBody().getUsername(), userTest.getUsername());
        assertEquals(response.getBody().getAddress().getGeo().getLat(), userTest.getAddress().getGeo().getLat());
        assertEquals(response.getBody().getAddress().getGeo().getLng(), userTest.getAddress().getGeo().getLng());
    }

    /**
     * Test of deleteUserById method, of class UserController.
     */
    @Test
    public void testDeleteUserById() {
        GeoDTO geo = new GeoDTO("lat", "lng");
        AddressDTO address = new AddressDTO("calle", "suite", "Pereira", "660001", geo);
        CompanyDTO company = new CompanyDTO("name", "Phra", "bs");
        UserDTO userTest = new UserDTO(1, "prueba", "prueba@mail.com", address, "154454", "www", company);
        String deleteMsg= "Delete";
        when(userService.deleteUserById(userTest.getId())).thenReturn(deleteMsg);
        ResponseEntity<String> response = userController.deleteUserById(userTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody(), deleteMsg);
    }
    
}
