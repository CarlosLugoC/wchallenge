/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.PostDTO;
import ar.com.wolox.demo.service.implement.PostServicesImp;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class PostControllerTest {
    
    @Mock
    private PostServicesImp postServices;
    
    @InjectMocks
    private PostController postController;
    
    @Before
    public void setUp() {
    }

    /**
     * Test of getPosts method, of class PostController.
     */
    @Test
    public void testGetPosts() {
        List<PostDTO> postListDTO = new ArrayList<>();
        PostDTO postTest = new PostDTO(1, 1, "title", "body");
        postListDTO.add(postTest);
        when(postServices.getAllPosts()).thenReturn(postListDTO);
        ResponseEntity<List<PostDTO>> response = postController.getAllPosts();
        assertNotNull(response);
        assertEquals(response.getBody(), postListDTO);
    }
    
    @Test
    public void testGetPostsNoContent() {
        List<PostDTO> postListDTO = new ArrayList<>();
        when(postServices.getAllPosts()).thenReturn(postListDTO);
        ResponseEntity<List<PostDTO>> response = postController.getAllPosts();
        assertNotNull(response);
        assertNull(response.getBody());
    }

    /**
     * Test of getPostById method, of class PostController.
     */
    @Test
    public void testGetPostById() {
        PostDTO postTest = new PostDTO(1, 1, "title", "body");
        when(postServices.getPostById(postTest.getId())).thenReturn(postTest);
        ResponseEntity<PostDTO> response = postController.getPostById(postTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody(), postTest);
    }
    
}
