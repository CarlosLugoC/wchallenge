/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.AddressDTO;
import ar.com.wolox.demo.dto.CompanyDTO;
import ar.com.wolox.demo.dto.PermitDTO;
import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.model.EPermission;
import ar.com.wolox.demo.service.implement.PermitServiceImp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class PermitControllerTest {
    
    @Mock
    private PermitServiceImp permitService;
    
    @Mock
    private ModelMapper modMapper;
    
    @InjectMocks
    private PermitController permitController;
    
    @Before
    public void setUp() {
    }

    /**
     * Test of createPermit method, of class PermitController.
     */
    @Test
    public void testCreatePermit() {
        Set<String> permissions = new HashSet<>();
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        when(permitService.createPermit(permissionDTOTest)).thenReturn(permissionDTOTest);
        ResponseEntity<PermitDTO> response = permitController.createPermit(permissionDTOTest);
        assertNotNull(response);
        assertEquals(response.getBody(), permissionDTOTest);
    }

    /**
     * Test of modifiyPermits method, of class PermitController.
     */
    @Test
    public void testUpdatePermits() {
        PermitDTO permissionDTOTest = new PermitDTO();
        Set<String> permissions= new HashSet<>();
        permissionDTOTest.setId(1);
        permissionDTOTest.setAlbumId(1);
        permissionDTOTest.setPermissions(permissions);
        permissionDTOTest.setUserId(1);
        when(permitService.updatePermit(permissionDTOTest.getId(),permissionDTOTest)).thenReturn(permissionDTOTest);
        ResponseEntity<PermitDTO> response = permitController.updatePermit(permissionDTOTest.getId(),permissionDTOTest);
        assertNotNull(response);
        assertEquals(response.getBody(), permissionDTOTest);
    }

    /**
     * Test of usersIdByPermits method, of class PermitController.
     */
    @Test
    public void testUsersIdByPermits() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        String permission = "WRITE";
        List<Integer> usersId = new ArrayList<>();
        usersId.add(permissionDTOTest.getUserId());
        when(permitService.getUsersIdByAlbumIdAndPermission(permissionDTOTest.getAlbumId(), permission)).thenReturn(usersId);
        ResponseEntity<List<Integer>> response = permitController.getUsersIdByPermit(permissionDTOTest.getAlbumId(), permission);
        assertNotNull(response);
        assertEquals(response.getBody(), usersId);
    }
    
    @Test
    public void testUsersIdByPermitsNoContent() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        String permission = "WRITE";
        List<Integer> usersId = new ArrayList<>();
        when(permitService.getUsersIdByAlbumIdAndPermission(permissionDTOTest.getAlbumId(), permission)).thenReturn(usersId);
        ResponseEntity<List<Integer>> response = permitController.getUsersIdByPermit(permissionDTOTest.getAlbumId(), permission);
        assertNotNull(response);
        assertNull(response.getBody());
    }
    /**
     * Test of usersByPermits method, of class PermitController.
     */
    @Test
    public void testUsersByPermits() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        String permission = "WRITE";
        AddressDTO address = new AddressDTO();
        CompanyDTO company = new CompanyDTO();
        UserDTO userTest = new UserDTO(1, "test", "test@mail.com", address, "154454", "www", company);
        List<UserDTO> users = new ArrayList<>();
        users.add(userTest);
        when(permitService.getUsersByAlbumIdAndPermission(permissionDTOTest.getAlbumId(), permission)).thenReturn(users);
        ResponseEntity<List<UserDTO>> response = permitController.getUsersByPermit(permissionDTOTest.getAlbumId(), permission);
        assertNotNull(response);
        assertEquals(response.getBody(), users);
    }
    
    @Test
    public void testUsersByPermitsNoContent() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        String permission = "WRITE";
        List<UserDTO> users = new ArrayList<>();
        when(permitService.getUsersByAlbumIdAndPermission(permissionDTOTest.getAlbumId(), permission)).thenReturn(users);
        ResponseEntity<List<UserDTO>> response = permitController.getUsersByPermit(permissionDTOTest.getAlbumId(), permission);
        assertNotNull(response);
        assertNull(response.getBody());
    }

    /**
     * Test of getAllPermits method, of class PermitController.
     */
    @Test
    public void testGetAllPermits() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        List<PermitDTO> permits = new ArrayList<>();
        permits.add(permissionDTOTest);
        Page<PermitDTO> responsePage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        when(permitService.getAllPermits(pageNumber,pageSize)).thenReturn(responsePage);
        ResponseEntity<Page<PermitDTO>> response = permitController.getAllPermits(pageNumber,pageSize);
        assertNotNull(response);
        assertEquals(response.getBody(), responsePage);
    }
    
    @Test
    public void testGetAllPermitsNoContent() {
        List<PermitDTO> permits = new ArrayList<>();
        Page<PermitDTO> responsePage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        when(permitService.getAllPermits(pageNumber,pageSize)).thenReturn(responsePage);
        ResponseEntity<Page<PermitDTO>> response = permitController.getAllPermits(pageNumber,pageSize);
        assertNotNull(response);
        assertNull(response.getBody());
    }

    /**
     * Test of getPermitsByUsers method, of class PermitController.
     */
    @Test
    public void testGetPermitsByUsers() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        List<PermitDTO> permits = new ArrayList<>();
        permits.add(permissionDTOTest);
        Page<PermitDTO> responsePage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        when(permitService.getPermitsByUser(permissionDTOTest.getUserId(),pageNumber,pageSize)).thenReturn(responsePage);
        ResponseEntity<Page<PermitDTO>> response = permitController.getPermitsByUsers(permissionDTOTest.getUserId(), pageNumber, pageSize);
        assertNotNull(response);
        assertEquals(response.getBody(), responsePage);
    }
    
    @Test
    public void testGetPermitsByUsersNoContent() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        List<PermitDTO> permits = new ArrayList<>();
        Page<PermitDTO> responsePage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        when(permitService.getPermitsByUser(permissionDTOTest.getUserId(),pageNumber,pageSize)).thenReturn(responsePage);
        ResponseEntity<Page<PermitDTO>> response = permitController.getPermitsByUsers(permissionDTOTest.getUserId(), pageNumber, pageSize);
        assertNotNull(response);
        assertNull(response.getBody());
    }

    /**
     * Test of getPermitsByAlbum method, of class PermitController.
     */
    @Test
    public void testGetPermitsByAlbum() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        List<PermitDTO> permits = new ArrayList<>();
        permits.add(permissionDTOTest);
        Page<PermitDTO> responsePage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        when(permitService.getPermitsByAlbum(permissionDTOTest.getAlbumId(),pageNumber,pageSize)).thenReturn(responsePage);
        ResponseEntity<Page<PermitDTO>> response = permitController.getPermitsByAlbum(permissionDTOTest.getAlbumId(),pageNumber,pageSize);
        assertNotNull(response);
        assertEquals(response.getBody(), responsePage);
    }
    
    @Test
    public void testGetPermitsByAlbumNoContent() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        List<PermitDTO> permits = new ArrayList<>();
        Page<PermitDTO> responsePage = new PageImpl<>(permits);
        Integer pageNumber = 0;
        Integer pageSize = 3;
        when(permitService.getPermitsByAlbum(permissionDTOTest.getAlbumId(),pageNumber,pageSize)).thenReturn(responsePage);
        ResponseEntity<Page<PermitDTO>> response = permitController.getPermitsByAlbum(permissionDTOTest.getAlbumId(),pageNumber,pageSize);
        assertNotNull(response);
        assertNull(response.getBody());
    }

    /**
     * Test of deletePermitById method, of class PermitController.
     */
    @Test
    public void testDeletePermitById() {
        Set<String> permissions = new HashSet<>();
        permissions.add("WRITE");
        PermitDTO permissionDTOTest = new PermitDTO(1, 1, permissions, 1);
        String deleteMsg = "Delete";
        when(permitService.deletePermitById(permissionDTOTest.getId())).thenReturn(deleteMsg);
        ResponseEntity<String> response = permitController.deletePermitById(permissionDTOTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody(), deleteMsg);
    }
    
}
