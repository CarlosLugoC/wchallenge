/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.PhotoDTO;
import ar.com.wolox.demo.service.implement.PhotoServiceImp;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class PhotoControllerTest {
    @Mock
    private PhotoServiceImp photoService;
    
    @InjectMocks
    private PhotoController photoController; 

    @Before
    public void setUp() {
    }
    /**
     * Test of getAllPhotos method, of class PhotoController.
     */
    @Test
    public void testGetAllPhotos() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnailurl", 24);
        List<PhotoDTO> photos = new ArrayList<>();
        photos.add(photoTest);
        when(photoService.getAllPhotos()).thenReturn(photos);
        ResponseEntity<List<PhotoDTO>> response = photoController.getAllPhotos();
        assertNotNull(response);
        assertEquals(response.getBody(), photos);
    }
    
    @Test
    public void testGetAllPhotosEmpty() {
        List<PhotoDTO> photos = new ArrayList<>();
        when(photoService.getAllPhotos()).thenReturn(photos);
        ResponseEntity<List<PhotoDTO>> response = photoController.getAllPhotos();
        assertNotNull(response);
    }

    /**
     * Test of getPhotosByAlbum method, of class PhotoController.
     */
    @Test
    public void testGetPhotosByAlbum() {
        PhotoDTO photoTest = new PhotoDTO();
        photoTest.setAlbumId(1);
        List<PhotoDTO> photos = new ArrayList<>();
        photos.add(photoTest);
        when(photoService.getPhotosByAlbumId(photoTest.getAlbumId())).thenReturn(photos);
        ResponseEntity<List<PhotoDTO>> response = photoController.getPhotosByAlbum(photoTest.getAlbumId());
        assertNotNull(response);
        assertEquals(response.getBody(), photos);
    }
    
    @Test
    public void testGetPhotosByAlbumEmpty() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnailurl", 24);
        List<PhotoDTO> photos = new ArrayList<>();
        when(photoService.getPhotosByAlbumId(photoTest.getAlbumId())).thenReturn(photos);
        ResponseEntity<List<PhotoDTO>> response = photoController.getPhotosByAlbum(photoTest.getAlbumId());
        assertNotNull(response);
    }

    /**
     * Test of getPhotosByUser method, of class PhotoController.
     */
    @Test
    public void testGetPhotosByUser() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnailurl", 24);
        List<PhotoDTO> photos = new ArrayList<>();
        photos.add(photoTest);
        when(photoService.getPhotosByUser(1)).thenReturn(photos);
        ResponseEntity<List<PhotoDTO>> response = photoController.getPhotosByUser(1);
        assertNotNull(response);
        assertEquals(response.getBody(), photos);
    }

    /**
     * Test of getPhotoById method, of class PhotoController.
     */
    @Test
    public void testGetPhotoById() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnailurl", 24);
        when(photoService.getPhotoById(photoTest.getId())).thenReturn(photoTest);
        ResponseEntity<PhotoDTO> response = photoController.getPhotoById(photoTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody().getThumbnailUrl(), photoTest.getThumbnailUrl());
    }

    /**
     * Test of addPhoto method, of class PhotoController.
     */
    @Test
    public void testAddPhoto() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnailurl", 24);
        when(photoService.createPhoto(photoTest)).thenReturn(photoTest);
        ResponseEntity<PhotoDTO> response = photoController.addPhoto(photoTest);
        assertNotNull(response);
        assertEquals(response.getBody().getTitle(), photoTest.getTitle());
    }

    /**
     * Test of modifyPhoto method, of class PhotoController.
     */
    @Test
    public void testUpdatePhoto() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnailurl", 24);
        when(photoService.updatePhoto(photoTest.getId(),photoTest)).thenReturn(photoTest);
        ResponseEntity<PhotoDTO> response = photoController.updatePhoto(photoTest.getId(),photoTest);
        assertNotNull(response);
        assertEquals(response.getBody().getUrl(), photoTest.getUrl());
    }

    /**
     * Test of deletePhotoById method, of class PhotoController.
     */
    @Test
    public void testDeletePhotoById() {
        PhotoDTO photoTest = new PhotoDTO(1, "title", "url", "thumbnailurl", 24);
        String deleteMsg = "Delete";
        when(photoService.deletePhotoById(photoTest.getId())).thenReturn(deleteMsg);
        ResponseEntity<String> response = photoController.deletePhotoById(photoTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody(), deleteMsg);
    }
    
}
