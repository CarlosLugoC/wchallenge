/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.AlbumDTO;
import ar.com.wolox.demo.service.implement.AlbumServiceImp;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author C-Lug
 */
@RunWith(SpringRunner.class)
public class AlbumControllerTest {

    @Mock
    private AlbumServiceImp albumService;
    
    @InjectMocks
    private AlbumController albumController;
    
    @Before
    public void setUp() {
    }

    /**
     * Test of getAllAlbums method, of class AlbumController.
     */
    @Test
    public void testGetAllAlbums() {
        AlbumDTO albumDTOTest = new AlbumDTO(1,"title",1);
        List<AlbumDTO> albums = new ArrayList<>();
        albums.add(albumDTOTest);
        when(albumService.getAllAlbums()).thenReturn(albums);
        ResponseEntity<List<AlbumDTO>> response = albumController.getAllAlbums();
        assertNotNull(response);
        assertEquals(response.getBody(), albums);
    }
    
    @Test
    public void testGetAllAlbumsEmpty() {
        List<AlbumDTO> albums = new ArrayList<>();
        when(albumService.getAllAlbums()).thenReturn(albums);
        ResponseEntity<List<AlbumDTO>> response = albumController.getAllAlbums();
        assertNotNull(response);
    }

    /**
     * Test of getAlbumsByUser method, of class AlbumController.
     */
    @Test
    public void testGetAlbumsByUser() {
        AlbumDTO albumDTOTest = new AlbumDTO(1, "title", 1);
        List<AlbumDTO> albums = new ArrayList<>();
        albums.add(albumDTOTest);
        when(albumService.getAlbumsByUser(albumDTOTest.getUserId())).thenReturn(albums);
        ResponseEntity<List<AlbumDTO>> response = albumController.getAlbumsByUser(albumDTOTest.getUserId());
        assertNotNull(response);
        assertEquals(response.getBody(), albums);
    }
    
    @Test
    public void testGetAlbumsByUserEmpty() {
        AlbumDTO albumDTOTest = new AlbumDTO(1, "title", 1);
        List<AlbumDTO> albums = new ArrayList<>();
        when(albumService.getAlbumsByUser(albumDTOTest.getUserId())).thenReturn(albums);
        ResponseEntity<List<AlbumDTO>> response = albumController.getAlbumsByUser(albumDTOTest.getUserId());
        assertNotNull(response);
    }

    /**
     * Test of getAlbumById method, of class AlbumController.
     */
    @Test
    public void testGetAlbumById() {
        AlbumDTO albumDTOTest = new AlbumDTO();
        albumDTOTest.setId(1);
        albumDTOTest.setTitle("title");
        albumDTOTest.setUserId(1);
        when(albumService.getAlbumById(albumDTOTest.getId())).thenReturn(albumDTOTest);
        ResponseEntity<AlbumDTO> response = albumController.getAlbumById(albumDTOTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody(), albumDTOTest);
    }

    /**
     * Test of addAlbum method, of class AlbumController.
     */
    @Test
    public void testAddAlbum() {
        AlbumDTO albumDTOTest = new AlbumDTO(1,"title",1);
        when(albumService.createAlbum(albumDTOTest)).thenReturn(albumDTOTest);
        ResponseEntity<AlbumDTO> response = albumController.addAlbum(albumDTOTest);
        assertNotNull(response);
        assertEquals(response.getBody().getTitle(), albumDTOTest.getTitle());
    }

    /**
     * Test of modifyAlbum method, of class AlbumController.
     */
    @Test
    public void testUpdateAlbum() {
        AlbumDTO albumDTOTest = new AlbumDTO(1,"title",1);
        when(albumService.updateAlbum(albumDTOTest.getId(), albumDTOTest)).thenReturn(albumDTOTest);
        ResponseEntity<AlbumDTO> response = albumController.updateAlbum(albumDTOTest.getId(), albumDTOTest);
        assertNotNull(response);
        assertEquals(response.getBody().getUserId(), albumDTOTest.getUserId());
    }

    /**
     * Test of deleteAlbumById method, of class AlbumController.
     */
    @Test
    public void testDeleteAlbumById() {
        AlbumDTO albumDTOTest = new AlbumDTO(1,"title",1);
        String deleteMsg = "Delete";
        when(albumService.deleteAlbumById(albumDTOTest.getId())).thenReturn(deleteMsg);
        ResponseEntity<String> response = albumController.deleteAlbumById(albumDTOTest.getId());
        assertNotNull(response);
        assertEquals(response.getBody(), deleteMsg);
    }
    
}
