/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.AlbumDTO;
import ar.com.wolox.demo.service.AlbumService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author C-Lug
 */
@RestController
@RequestMapping("api/wChallenge/albums")
@Api(value = "Album microservice", tags = "This Service REST has a CRUD for Albums")
public class AlbumController {
    
    @Autowired
    private AlbumService albumService;
    
    /**
     * This method is used to get a list with all the albums
     * @return ResponseEntity.ok(List AlbumDTO), in case that the were not found an album, the method return a ResponseEntity.noContent
     */
    @ApiOperation(value = "Get all Albums")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched all Album"),
        @ApiResponse(code = 204, message = "Successfully search List Album but no found any album"),
        @ApiResponse(code = 400, message = "An error happened during the search")
        
    })
    @GetMapping
    public ResponseEntity<List<AlbumDTO>> getAllAlbums() {
        List<AlbumDTO> albums = albumService.getAllAlbums();
        if (albums.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(albums);
    }
    
    /**
     * This method is used to get a list of albums by the id of an user 
     * @param userId this is the id of the user who is going to used to make the search
     * @return ResponseEntity.ok(List AlbumDTO), in case that the were not found an album, the method return a ResponseEntity.notFound
     */
    @ApiOperation(value = "Get a List Albums by UserId")
    @GetMapping("/byUser/{id}")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched List Album by UserId"),
        @ApiResponse(code = 204, message = "Successfully search List Album by UserId but no found any album"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    public ResponseEntity<List<AlbumDTO>> getAlbumsByUser(
            @ApiParam( value = "ID of User you need to search a list of albums by user", required = true, example = "1")
            @PathVariable("id") Integer userId) {
        List<AlbumDTO> albumsUser = albumService.getAlbumsByUser(userId);
        if (albumsUser.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(albumsUser);
    }
    
    /**
     * This method is used to get an album by the id
     * @param id this is the id of the album to make the search
     * @return ResponseEntity.ok(AlbumDTO)
     */
    @ApiOperation(value = "Get an Album by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully found Album"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/{id}")
    public ResponseEntity<AlbumDTO> getAlbumById(
            @ApiParam( value = "ID of Album thats need to be searched", required = true, example = "5")
            @PathVariable("id") Integer id) {
        AlbumDTO album = albumService.getAlbumById(id);
        return ResponseEntity.ok(album);
    }
    
    /**
     * This method is used to create an album
     * @param albumDTO this is the object AlbumDTO who is going to be created
     * @return ResponseEntity.ok(AlbumDTO)
     */
    @ApiOperation(value = "Add an Album")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully created Album"),
        @ApiResponse(code = 409, message = "Album is already Created"),
        @ApiResponse(code = 400, message = "An error happened during the creation")
    })
    @PostMapping
    public ResponseEntity<AlbumDTO> addAlbum(
            @ApiParam( value = "Album object to be added", required = true)
            @RequestBody AlbumDTO albumDTO) {
        AlbumDTO album = albumService.createAlbum(albumDTO);
        return ResponseEntity.ok(album);
    }
    
    /**
     * This method is used to update an album
     * @param albumId this is the id of the album whois going to be updated
     * @param albumDTO this is the object AlbumDTO who is going to be updated
     * @return ResponseEntity.ok(AlbumDTO)
     */
    @ApiOperation(value = "Update an Album")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully updated Album"),
        @ApiResponse(code = 404, message = "Album not Found"),
        @ApiResponse(code = 409, message = "Relation User and Album is already set in another Permit"),
        @ApiResponse(code = 400, message = "An error happened during the update")
    })
    @PutMapping("/{id}")
    public ResponseEntity<AlbumDTO> updateAlbum(
            @ApiParam( value = "ID of Album that needs to be updated", required = true, example = "100")
            @PathVariable("id") Integer albumId,
            @ApiParam( value = "Album object to be updated", required = true)
            @RequestBody AlbumDTO albumDTO) {
        AlbumDTO album = albumService.updateAlbum(albumId, albumDTO);
        return ResponseEntity.ok(album);
    }
    
    /**
     * This method is used to delete an album
     * @param albumId this is the id of the album whois going to be deleted
     * @return ResponseEntity.ok(String)
     */
    @ApiOperation(value = "Delete an Album by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully deleted Album"),
        @ApiResponse(code = 404, message = "Album not Found"),
        @ApiResponse(code = 400, message = "An error happened during the delete")
    })
    @DeleteMapping("/{id}")
    public  ResponseEntity<String> deleteAlbumById(
            @ApiParam( value = "ID of Album that needs to be deleted", required = true, example = "1")
            @PathVariable("id") Integer albumId) {
        String response = albumService.deleteAlbumById(albumId);
        return ResponseEntity.ok(response);
    }
}
