/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.PhotoDTO;
import ar.com.wolox.demo.service.PhotoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author C-Lug
 */
@RestController
@RequestMapping("api/wChallenge/photos")
@Api(value = "Photo microservice", tags = "This Service REST has a CRUD for Photos")
public class PhotoController {
    
    @Autowired
    private PhotoService photoService;
    
    /**
     * This method is used to get a list with all the photos
     * @return ResponseEntity.ok(List PhotoDTO)
     */ 
    @ApiOperation(value = "Get all Photos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched all Photos"),
        @ApiResponse(code = 204, message = "Successfully search but no found any Photo"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping
    public ResponseEntity<List<PhotoDTO>> getAllPhotos(){
        List<PhotoDTO> photos = photoService.getAllPhotos();
        if (photos.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(photos);
    }
    
    /**
     * This method is used to get a list photos by the id of an album
     * @param albumId this is the id of the album to make the search
     * @return ResponseEntity.ok(List PhotoDTO)
     */
    @ApiOperation(value = "Get a List Photos by album ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched List Photos by album ID"),
        @ApiResponse(code = 204, message = "Successfully search but no found any Photo"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/byAlbum/{id}")
    public ResponseEntity<List<PhotoDTO>> getPhotosByAlbum(
            @ApiParam( value = "ID of album that needs to be searched the Photos by albumId", required = true, example = "2")
            @PathVariable("id") Integer albumId){
        List<PhotoDTO> photosAlbum = photoService.getPhotosByAlbumId(albumId);
        if (photosAlbum.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(photosAlbum);
    }
    
    /**
     * This method is used to get a list photos byt the id of an user
     * @param userId this is the id of the user to make the search
     * @return ResponseEntity.ok(List PhotoDTO)
     */
    @ApiOperation(value = "Get a List Photos by user ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched List Photos by user ID"),
        @ApiResponse(code = 204, message = "Successfully search but no found any Photo"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/byUser/{id}")
    public ResponseEntity<List<PhotoDTO>> getPhotosByUser(
            @ApiParam( value = "ID of user that needs to be search the Photos by userId", required = true, example = "2")
            @PathVariable("id") Integer userId){
        List<PhotoDTO> photosUser = photoService.getPhotosByUser(userId);
        return ResponseEntity.ok(photosUser);
    }
    
    /**
     * This method is used to get a photo by the id
     * @param id this is the id of the photo to make the search
     * @return ResponseEntity.ok(PhotoDTO)
     */
    @ApiOperation(value = "Get a Photo by  ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched Photo by ID"),
        @ApiResponse(code = 404, message = "Not Found any Photo by ID given")
    })
    @GetMapping("/{id}")
    public ResponseEntity<PhotoDTO> getPhotoById(
            @ApiParam( value = "ID of Photo that needs to be searched", required = true, example = "2")
            @PathVariable("id") Integer id){
        PhotoDTO photo = photoService.getPhotoById(id);
        return ResponseEntity.ok(photo);
    }
    
    /**
     * This method is used to create a comment
     * @param photoDTO this is the object PhotoDTO who is going to be created
     * @return ResponseEntity.ok(PhotoDTO)
     */
    @ApiOperation(value = "Add a Photo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully created Photo"),
        @ApiResponse(code = 409, message = "Photo is already Created"),
        @ApiResponse(code = 400, message = "An error happened during the creation")
    })
    @PostMapping
    public ResponseEntity<PhotoDTO> addPhoto(
            @ApiParam(value = "Photo object who is going to be created", required = true)
            @RequestBody PhotoDTO photoDTO) {
        PhotoDTO photo = photoService.createPhoto(photoDTO);
        return ResponseEntity.ok(photo);
    }
    
    /**
     * This method is used to update a comment
     * @param photoId this is the id of the photo who is going to be update
     * @param photoDTO this is the object PhotoDTO who is going to be updated
     * @return ResponseEntity.ok(PhotoDTO)
     */
    @ApiOperation(value = "Update a Photo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully updated Photo"),
        @ApiResponse(code = 404, message = "Photo not Found "),
        @ApiResponse(code = 400, message = "An error happened during the update")
    })
    @PutMapping("/{id}")
    public ResponseEntity<PhotoDTO> updatePhoto(
            @ApiParam(value = "ID of Photo who is going to be updated", required = true, example = "1000")
            @PathVariable("id") Integer photoId, 
            @ApiParam(value = "Photo object who is going to be updated", required = true)
            @RequestBody PhotoDTO photoDTO) {
        PhotoDTO photo = photoService.updatePhoto(photoId, photoDTO);
        return ResponseEntity.ok(photo);
    }
    
    /**
     * This method is used to delete a photo
     * @param photoId this is the id of the photo who is going to be deleted
     * @return ResponseEntity.ok(String)
     */
    @ApiOperation(value = "Delete a Photo by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully deleted Photo"),
        @ApiResponse(code = 404, message = "Photo not Found"),
        @ApiResponse(code = 400, message = "An error happened during the delete")
    })
    @DeleteMapping("/{id}")
    public  ResponseEntity<String> deletePhotoById(
            @ApiParam( value = "ID of Photo that needs to be deleted", required = true, example = "10")
            @PathVariable("id") Integer photoId) {
        String response = photoService.deletePhotoById(photoId);
        return ResponseEntity.ok(response);
    }
}
