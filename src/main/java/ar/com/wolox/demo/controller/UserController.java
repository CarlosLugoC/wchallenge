/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author C-Lug
 */
@RestController
@RequestMapping("api/wChallenge/users")
@Api(value = "User microservice", tags = "This Service REST has a CRUD for Users")
public class UserController {
    
    @Autowired
    private UserService userService;
    
    /**
     * This method is used to get a list with all the users
     * @return ResponseEntity.ok(List UserDTO)
     */
    @ApiOperation(value = "Get all Users")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched all Users"),
        @ApiResponse(code = 204, message = "Successfully search but no found any use"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<UserDTO> users = userService.getAllUsers();
        if (users.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(users);
    }
    
    /**
     * This method is used to get a user by the id
     * @param id this is the id of the user to make the search
     * @return ResponseEntity.ok(UserDTO)
     */
    @ApiOperation(value = "Get an User by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched a List Users by ID"),
        @ApiResponse(code = 404, message = "User not Found by the ID given")
    })
    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUserById(
            @ApiParam(value = "ID of the user to make the search", required = true, example = "10")
            @PathVariable("id") Integer id) {
        UserDTO user = userService.getUserById(id);
        return ResponseEntity.ok(user);
    }
    
    /**
     * This method is used to create an user
     * @param userDTO this is the id object UserDTO who is going to be created
     * @return ResponseEntity.ok(UserDTO)
     */
    @ApiOperation(value = "Add an User")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully created User"),
        @ApiResponse(code = 409, message = "User is already Created"),
        @ApiResponse(code = 400, message = "An error happened during the creation")
    })
    @PostMapping
    public ResponseEntity<UserDTO> addUser(
            @ApiParam(value = "User object who is going to be created", required = true)
            @RequestBody UserDTO userDTO) {
        UserDTO user = userService.createUser(userDTO);
        return ResponseEntity.ok(user);
    }
    
    /**
     * This method is used to update an user
     * @param userId this is the id of the user who is going to be updated
     * @param userDTO this is the id object UserDTO who is going to be updated
     * @return ResponseEntity.ok(UserDTO)
     */
    @ApiOperation(value = "Update an User")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully updated User"),
        @ApiResponse(code = 404, message = "User not Found"),
        @ApiResponse(code = 400, message = "An error happened during the creation")
    })
    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> updateUser(
            @ApiParam(value = "ID of User who is going to be updated", required = true, example = "10")
            @PathVariable("id") Integer userId, 
            @ApiParam(value = "User object who is going to be updated", required = true)
            @RequestBody UserDTO userDTO) {
        UserDTO user = userService.updateUser(userId, userDTO);
        return ResponseEntity.ok(user);
    }
    
    /**
     * This method is used to delete an user
     * @param userId this is the id of the user who is going to be deleted
     * @return ResponseEntity.ok(String)
     */
    @ApiOperation(value = "Delte an User by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully deleted User"),
        @ApiResponse(code = 404, message = "User not Found"),
        @ApiResponse(code = 400, message = "An error happened during the delete")
    })
    @DeleteMapping("/{id}")
    public  ResponseEntity<String> deleteUserById(
            @ApiParam( value = "ID of User that needs to be deleted", required = true, example = "10")
            @PathVariable("id") Integer userId) {
        String response = userService.deleteUserById(userId);
        return ResponseEntity.ok(response);
    }
    
}
