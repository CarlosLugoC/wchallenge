/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.CommentDTO;
import ar.com.wolox.demo.service.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author C-Lug
 */
@RestController
@RequestMapping("api/wChallenge/comments")
@Api(value = "Comment microservice", tags = "This Service REST has a CRUD for Comments")
public class CommentController {
    
    @Autowired
    private CommentService commentService;
    
    /**
     *  This method is used to get a list with all the comments
     * @return ResponseEntity.ok(List CommentDTO), in case that the were not found a comment, the method return a ResponseEntity.noContent
     */
    @ApiOperation(value = "Get all Comments")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched all Comments"),
        @ApiResponse(code = 204, message = "Successfully search List Comments but no found any comment"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping
    public ResponseEntity<List<CommentDTO>> getAllComments() {
        List<CommentDTO> comments = commentService.getAllComments();
        if(comments.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(comments);
    }
    
    /**
     * This method is used to get a comment by the ID
     * @param id this is the ID to make the search
     * @return ResponseEntity.ok(CommentDTO)
     */
    @ApiOperation(value = "Get a Comment by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched a Comment by ID"),
        @ApiResponse(code = 404, message = "Comment not found by ID")
    })
    @GetMapping("/{id}")
    public ResponseEntity<CommentDTO> getCommentById(
            @ApiParam( value = "ID of comment thats need to be searched", required = true, example = "100")
            @PathVariable("id") Integer id) {
        CommentDTO commentByName = commentService.getCommentById(id);
        return ResponseEntity.ok(commentByName);
    }
    
    /**
     * This method is used to get a comment by the name
     * @param name this is the name to make the search
     * @return ResponseEntity.ok(List CommentDTO), in case that the were not found a comment, the method return a ResponseEntity.noContent
     */
    @ApiOperation(value = "Get a List Comment by Name")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched a List Comment by Name"),
        @ApiResponse(code = 204, message = "Successfully search List Comments by Name but no found any comment"),
        @ApiResponse(code = 400, message = "Comment not found by Name")
    })
    @GetMapping("/byName")
    public ResponseEntity<List<CommentDTO>> getCommentByName(
            @ApiParam( value = "Name of comment thats need to be searched", required = true, example = "quo vero reiciendis velit similique earum")
            @RequestParam("name") String name) {
        List<CommentDTO> commentsByName = commentService.getCommentByNameV2(name);
        if (commentsByName.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(commentsByName);
    }
    
    /**
     * This method is used to get a List comment by the id of an user
     * @param id this is the id of the user to make the search
     * @return ResponseEntity.ok(List CommentDTO), in case that the were not found a comment, the method return a ResponseEntity.notFound
     */
    @ApiOperation(value = "Get a Comment by User ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched Comments by user ID"),
        @ApiResponse(code = 204, message = "Successfully search List Comments by user Id but no found any comment"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/byUser/{id}")
    public ResponseEntity<List<CommentDTO>> getCommentsByUserId(
            @ApiParam( value = "ID of User thats need to be searched the comment", required = true, example = "1")
            @PathVariable("id") Integer id) {
        List<CommentDTO> commentsByUserId = commentService.getCommentsByUserId(id);
        if (commentsByUserId.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(commentsByUserId);
    }
    
    /**
     * This method is used to get a List comment by the email
     * @param email this is the email to make the search
     * @return ResponseEntity.ok(List CommentDTO), in case that the were not found a comment, the method return a ResponseEntity.noContent
     */
    @ApiOperation(value = "Get a List Comment by Email")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched Comments by Email"),
        @ApiResponse(code = 204, message = "Successfully search List Comments by Email but no found any comment"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/byEmail")
    public ResponseEntity<List<CommentDTO>> getCommentsByEmail(
            @ApiParam( value = "Email of made the comment thats need to be searched", required = true, example = "Jayne_Kuhic@sydney.com")
            @RequestParam("email") String email) {
        List<CommentDTO> commentsByUserEmail = commentService.getCommentsByEmail(email);
        if (commentsByUserEmail.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(commentsByUserEmail);
    }
    
    /**
     * This method is used to create a comment
     * @param commentDTO this is the object CommentDTO who is going to be created
     * @return ResponseEntity.ok(CommentDTO)
     */
    @ApiOperation(value = "Add a Comment")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully created Comment"),
        @ApiResponse(code = 409, message = "Comment is already Created"),
        @ApiResponse(code = 400, message = "An error happened during the creation")
    })
    @PostMapping
    public ResponseEntity<CommentDTO> addComment(
            @ApiParam( value = "Comment object to be added", required = true)
            @RequestBody CommentDTO commentDTO) {
        CommentDTO comment = commentService.createComment(commentDTO);
        return ResponseEntity.ok(comment);
    }
    
    /**
     * This method is used to update a comment
     * @param commentId this is the id of the comment who is going to be updated
     * @param commentDTO this is the object CommentDTO who is going to be updated
     * @return ResponseEntity.ok(CommentDTO)
     */
    @ApiOperation(value = "Update a Comment")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully updated Comment"),
        @ApiResponse(code = 404, message = "Comment not Found"),
        @ApiResponse(code = 400, message = "An error happened during the update")
    })
    @PutMapping("/{id}")
    public ResponseEntity<CommentDTO> updateComment(
            @ApiParam( value = "ID of Comment that needs to be updated", required = true, example = "100")
            @PathVariable("id") Integer commentId, 
            @ApiParam( value = "Comment object to be added", required = true)
            @RequestBody CommentDTO commentDTO) {
        CommentDTO comment = commentService.updateComment(commentId, commentDTO);
        return ResponseEntity.ok(comment);
    }
    
    /**
     * This method is used to delete a comment
     * @param commentId this is the id of the comment who is going to be deleted
     * @return ResponseEntity.ok(String)
     */
    @ApiOperation(value = "Delete a Comment by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully deleted Comment"),
        @ApiResponse(code = 404, message = "Comment not Found"),
        @ApiResponse(code = 400, message = "An error happened during the delete")
    })
    @DeleteMapping("/{id}")
    public  ResponseEntity<String> deleteCommentById(
            @ApiParam( value = "ID of Comment that needs to be deleted", required = true, example = "2")
            @PathVariable("id") Integer commentId) {
        String response = commentService.deleteCommentById(commentId);
        return ResponseEntity.ok(response);
    }
    
}
