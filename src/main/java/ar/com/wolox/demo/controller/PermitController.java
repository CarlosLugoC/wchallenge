/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.PermitDTO;
import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.service.PermitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author C-Lug
 */
@RestController
@RequestMapping("api/wChallenge/permits")
@Api(value = "Permit microservice", tags = "This Service REST has a CRUD for Permits")
public class PermitController {
    
    @Autowired
    private PermitService permitService;
    
    /**
     * This method is used to create a permit 
     * @param permissionDTO this is the object PermitDTO who is going to be created
     * @return ResponseEntity.ok(PermitDTO)
     */
    @ApiOperation(value = "Add a Permit to db")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully created Permit"),
        @ApiResponse(code = 400, message = "An error happened during the creation"),
        @ApiResponse(code = 409, message = "Permit is already Created with that user and album")
    })
    @PostMapping
    public ResponseEntity<PermitDTO> createPermit(
            @ApiParam( value = "Permit object who is going to be created", required = true)
            @RequestBody PermitDTO permissionDTO) {
        PermitDTO permitResponse = permitService.createPermit(permissionDTO);
        return ResponseEntity.ok(permitResponse);
    }
    
    /**
     * This method is used to update a permit 
     * @param permissionId this is the id of the permit who is going to be created
     * @param permissionDTO this is the object PermitDTO who is going to be updated
     * @return ResponseEntity.ok(PermitDTO)
     */
    @ApiOperation(value = "Update a Permit in db")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully updated Permit"),
        @ApiResponse(code = 400, message = "An error happened during the update"),
        @ApiResponse(code = 404, message = "Permit not Found"),
        @ApiResponse(code = 409, message = "Permit is already Created with that user and album")
    })
    @PutMapping("/{id}")
    public ResponseEntity<PermitDTO> updatePermit(
            @ApiParam( value = "ID of Permit that needs to be updated", required = true, example = "1")
            @PathVariable("id") Integer permissionId, 
            @ApiParam( value = "Permit object who is going to be updated", required = true)
            @RequestBody PermitDTO permissionDTO) {
        PermitDTO permitResponse = permitService.updatePermit(permissionId, permissionDTO);
        return ResponseEntity.ok(permitResponse);
    }
    
    /**
     * This method is used to get a List Integer with the id of users by an album Id and a permission
     * @param albumId this is the id of the album to make the search
     * @param permission this is the permission to make the search
     * @return ResponseEntity.ok(List Integer)
     */
    @ApiOperation(value = "Get a List Integer with the users ID by an album Id and a permission")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched List Integer with the users ID by an album Id and a permission"),
        @ApiResponse(code = 204, message = "Successfully search List Integer with the users ID by an album Id and a permission but no found any user"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/usersId")
    public ResponseEntity<List<Integer>> getUsersIdByPermit(
            @ApiParam( value = "ID of Album that needs to be searched the Users Id", required = true, example = "2")
            @RequestParam("albumId") Integer albumId, 
            @ApiParam( value = "PERMISSION that needs to be searched the Users Id", required = true, example = "READ")
            @RequestParam("permission") String permission) {
        List<Integer> usersId = permitService.getUsersIdByAlbumIdAndPermission(albumId, permission);
        if (usersId.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(usersId);
    }
    
        /**
     * This method is used to get a List UsertDTO with the users by an album Id and a permission 
     * @param albumId this is the id of the album to make the search
     * @param permission this is the permission to make the search
     * @return ResponseEntity.ok(List Integer)
     */
    @ApiOperation(value = "Get a List UserDTO with the users by an album Id and a permission")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched List UserDTO with the users by an album Id and a permission"),
        @ApiResponse(code = 204, message = "Successfully search List UserDTO with the users by an album Id and a permission but no found any user"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getUsersByPermit(
            @ApiParam( value = "ID of Album that needs to be searched the Users", required = true, example = "2")
            @RequestParam("albumId") Integer albumId, 
            @ApiParam( value = "PERMISSION that needs to be searched the Users ", required = true, example = "READ")
            @RequestParam("permission") String permission) {
        List<UserDTO> usersId = permitService.getUsersByAlbumIdAndPermission(albumId, permission);
        if (usersId.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(usersId);
    }
    
    /**
     * This method is used to get a List of all the permits
     * @param pageNumber this is the number of the page where is 
     * @param pageSize this is the size of the page
     * @return ResponseEntity.ok(List PermitDTO)
     */
    @ApiOperation(value = "Get all Permits")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched all Permits"),
        @ApiResponse(code = 204, message = "Successfully search but no found any Permit"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping
    public ResponseEntity<Page<PermitDTO>> getAllPermits(
            @ApiParam( value = "number Page", required = true, example = "1")
            @RequestParam Integer pageNumber, 
            @ApiParam( value = "size Page", required = true, example = "3")
            @RequestParam Integer pageSize) {
        Page<PermitDTO> permitsResponse = permitService.getAllPermits(pageNumber, pageSize);
        if (permitsResponse.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(permitsResponse);
    }
    
    /**
     * This method is used to get a List of permits by the id of an user
     * @param userId this is the id of the user to make the search
     * @param pageNumber this is the number of the page where is 
     * @param pageSize this is the size of the page
     * @return ResponseEntity.ok(List PermitDTO)
     */
    @ApiOperation(value = "Get a List Permits by user ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched List Permits by user ID"),
        @ApiResponse(code = 204, message = "Successfully search but no found any Permit"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/byUser/{id}")
    public ResponseEntity<Page<PermitDTO>> getPermitsByUsers(
            @ApiParam( value = "ID of User that needs to be search the Permits by userId", required = true, example = "2")
            @PathVariable("id") Integer userId,
            @ApiParam( value = "number Page", required = true, example = "1")
            @RequestParam Integer pageNumber, 
            @ApiParam( value = "size Page", required = true, example = "3")
            @RequestParam Integer pageSize) {
        Page<PermitDTO> permitsResponse = permitService.getPermitsByUser(userId, pageNumber, pageSize);
        if (permitsResponse.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(permitsResponse);
    }
    
    /**
     * This method is used to get a List of permits by the id of an album
     * @param albumId this is the id of the album to make the search
     * @param pageNumber this is the number of the page where is 
     * @param pageSize this is the size of the page
     * @return ResponseEntity.ok(List PermitDTO)
     */
    @ApiOperation(value = "Get a List Permits by album ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully search List Permits by album ID"),
        @ApiResponse(code = 204, message = "Successfully search but no found any Permit"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/byAlbum/{id}")
    public ResponseEntity<Page<PermitDTO>> getPermitsByAlbum(
            @ApiParam( value = "ID of Album that needs to be search the Permits by albumId", required = true, example = "1")
            @PathVariable("id") Integer albumId,
            @ApiParam( value = "number Page", required = true, example = "1")
            @RequestParam Integer pageNumber, 
            @ApiParam( value = "size Page", required = true, example = "3")
            @RequestParam Integer pageSize) {
        Page<PermitDTO> permitsResponse = permitService.getPermitsByAlbum(albumId,pageNumber, pageSize);
        if (permitsResponse.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(permitsResponse);
    }
    
    /**
     * This method is used to get delete a permit by id
     * @param id this is the id of the permit who is going to be delete
     * @return ResponseEntity.ok(String)
     */
    @ApiOperation(value = "Delete a Permit by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully deleted Permit"),
        @ApiResponse(code = 404, message = "Permit not Found"),
        @ApiResponse(code = 400, message = "An error happened during the delete")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePermitById(
            @ApiParam( value = "ID of Permit that needs to be deleted", required = true, example = "2")
            @PathVariable("id") Integer id){
        
        String response = permitService.deletePermitById(id);
        return ResponseEntity.ok(response);
    }
}
