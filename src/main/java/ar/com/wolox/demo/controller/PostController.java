/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.controller;

import ar.com.wolox.demo.dto.PostDTO;
import ar.com.wolox.demo.service.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author C-Lug
 */
@RestController
@RequestMapping("/api/wChallenge/posts")
@Api(value = "Post microservice", tags = "This Service REST has a method Get for Posts")
public class PostController {
    @Autowired
    private PostService postService;
    
    /**
     * This method is used to get a list with all the posts
     * @return ResponseEntity.ok(List AlbumDTO), in case that the were not found a post, the method return a ResponseEntity.noContent
     */
    @ApiOperation(value = "Get all Posts")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully searched all Posts"),
        @ApiResponse(code = 204, message = "Successfully search List Post but no found any post"),
        @ApiResponse(code = 400, message = "An error happened during the search")
        
    })
    @GetMapping()
    public ResponseEntity<List<PostDTO>> getAllPosts() {
        List<PostDTO> posts = postService.getAllPosts();
        if (posts.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(posts);
    }
    
    /**
     * This method is used to get a post by the id
     * @param id this is the id of the post to make the search
     * @return ResponseEntity.ok(PostDTO)
     */
    @ApiOperation(value = "Get an Post by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully found Post"),
        @ApiResponse(code = 400, message = "An error happened during the search")
    })
    @GetMapping("/{id}")
    public ResponseEntity<PostDTO> getPostById(
            @ApiParam(value = "ID of Post thats need to be searched", required = true, example = "1")
            @PathVariable("id") Integer id) {
        PostDTO post = postService.getPostById(id);
        return ResponseEntity.ok(post);
    }
}
