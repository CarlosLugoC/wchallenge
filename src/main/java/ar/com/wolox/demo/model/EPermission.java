/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.model;

/**
 *
 * @author C-Lug
 */
public enum EPermission {
    WRITE,
    READ
}
