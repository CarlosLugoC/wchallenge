/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author C-Lug
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    @ApiModelProperty(notes = "The external api generated user ID",
            example = "10")
    private Integer id;
    @ApiModelProperty(notes = "The username of the user",
            example = "Username Example")
    private String username;
    @ApiModelProperty(notes = "The email of the user",
            example = "Email@Example.com")
    private String email;
    @ApiModelProperty(notes = "The Address of the user")
    private AddressDTO address;
    @ApiModelProperty(notes = "The phone of the user",
            example = "31015748")
    private String phone;
    @ApiModelProperty(notes = "The website of the user",
            example = "www.example.com")
    private String website;
    @ApiModelProperty(notes = "The Company of the user")
    private CompanyDTO company;
}
