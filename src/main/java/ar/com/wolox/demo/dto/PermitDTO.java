/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author C-Lug
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PermitDTO {
    @ApiModelProperty(notes = "The database generated permit ID",
            example = "1")
    private Integer id;
    @ApiModelProperty(notes = "The album ID", required = true,
            example = "100")
    private Integer albumId;
    @ApiModelProperty(notes = "The permissions on the album by the user",
            example = "[READ]")
    private Set<String> permissions = new HashSet<>();
    @ApiModelProperty(notes = "The user ID", required = true,
            example = "1")
    private Integer userId;
}
