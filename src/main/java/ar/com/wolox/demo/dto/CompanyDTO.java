/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author C-Lug
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDTO {
    @ApiModelProperty(notes = "The name of the company",
            example = "Company name Example")
    private  String name;
    @ApiModelProperty(notes = "The catchPhrase of the company",
            example = "CatchPhrase Example")
    private  String catchPhrase;
    @ApiModelProperty(notes = "The bs of the company",
            example = "Bs Example")
    private  String bs;
}
