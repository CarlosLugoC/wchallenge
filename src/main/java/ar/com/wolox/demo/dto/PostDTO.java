/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author C-Lug
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {
    @ApiModelProperty(notes = "The user ID who made the post",
            example = "1")
    private Integer userId;
    @ApiModelProperty(notes = "The external api generated post ID",
            example = "10")
    private Integer id;
    @ApiModelProperty(notes = "The title of the post",
            example = "Title Example")
    private String title;
    @ApiModelProperty(notes = "The body of the post",
            example = "Body Example")
    private String body;
}
