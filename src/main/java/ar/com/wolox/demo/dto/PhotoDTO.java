/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author C-Lug
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PhotoDTO {
    @ApiModelProperty(notes = "The external api generated Photo ID",
            example = "1000")
    private Integer id;
    @ApiModelProperty(notes = "The title of the Photo",
            example = "Title Example")
    private String title;
    @ApiModelProperty(notes = "The url of the Photo",
            example = "https://via.placeholder.com/600/92c952")
    private String url;
    @ApiModelProperty(notes = "The thumbnail of the Photo",
            example = "https://via.placeholder.com/600/92c952")
    private String thumbnailUrl;
    @ApiModelProperty(notes = "The album ID of the photo", required = true,
            example = "100")
    private Integer albumId;
}
