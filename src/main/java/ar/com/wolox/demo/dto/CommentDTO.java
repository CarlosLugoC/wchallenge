/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author C-Lug
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO {
    @ApiModelProperty(notes = "The external api generated comment ID",
            example = "100")    
    private Integer id;
    @ApiModelProperty(notes = "The name of the comment",
            example = "Name Example")
    private String name;
    @ApiModelProperty(notes = "The email of who make the comment",
            example = "example@example.com")
    private String email;
    @ApiModelProperty(notes = "The body of the comment",
            example = "Body Example")
    private String body;
    @ApiModelProperty(notes = "The ID of the post where was made the comment", required = true,
            example = "1")
    private Integer postId;
}
