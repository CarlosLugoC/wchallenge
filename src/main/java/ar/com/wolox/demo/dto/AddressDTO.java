/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author C-Lug
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AddressDTO {
    @ApiModelProperty(notes = "The street of the address",
            example = "Street Example")
    private String street;
    @ApiModelProperty(notes = "The suite of the address",
            example = "Suite Example")
    private String suite;
    @ApiModelProperty(notes = "The city of the address",
            example = "City Example")
    private String city;
    @ApiModelProperty(notes = "The zipcode of the address",
            example = "Zipcode Example")
    private String zipcode;
    @ApiModelProperty(notes = "The geo Localization of the address")
    private GeoDTO geo;

}
