/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.PostDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import ar.com.wolox.demo.service.PostService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@Service
public class PostServicesImp implements PostService{

    private static final String URL = "https://jsonplaceholder.typicode.com/posts";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String OBJECT = "post";
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private MessagesInternationalizationImp msg;
    
    /**
     * This method is used to get all the posts in the external api
     * @return List PostDTO in case there were not found a post, the method return an empty list,
     * @throws BadRequestException in case that pass an error during the search
     */
    @Override
    public List<PostDTO> getAllPosts() {
        try {
            ResponseEntity<List<PostDTO>> response = restTemplate.exchange(URL, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<PostDTO>>() {} );
            return response.getBody();
        }catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get a post by the id
     * @param id this is the id of the post to make the search
     * @return PostDTO with the object searched
     * @throws NotFoundException in case there was not found a post for that id
     */
    @Override
    public PostDTO getPostById(Integer id) {
        try {
            String url = String.format(STRINGFORMATSLASH,URL,id);
            ResponseEntity<PostDTO> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<PostDTO>() {} );
            return response.getBody();
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, id));
        }
    }
}
