/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;


import ar.com.wolox.demo.dto.PermitDTO;
import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import ar.com.wolox.demo.model.EPermission;
import ar.com.wolox.demo.model.Permit;
import ar.com.wolox.demo.service.PermitService;
import ar.com.wolox.demo.service.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import ar.com.wolox.demo.repository.PermitRepository;
import ar.com.wolox.demo.service.AlbumService;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author C-Lug
 */
@Service
public class PermitServiceImp implements PermitService {

    private static final String OBJECT = "permit";
    private static final String OBJECTPERMISSIONS = "permissions";
    
    @Autowired
    private PermitRepository permitRepository;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private AlbumService albumService;
    
    @Autowired
    private MessagesInternationalizationImp msg;
    
    @Autowired
    private ModelMapper modMapper;
    
    /**
     * This method is used to verified if the string is present like enum
     * @param str this is the string to make the comparision
     * @return EPermission in case that te comparision return true, in case that the comparision return false the method return a null
     */
    public static EPermission isEnumPresent(String str) {
        for (EPermission en : EPermission.values()) {
            if (en.name().equalsIgnoreCase(str))
                return en;
        }
        return null;
    }
    
    /**
     * This method is used to create a Permit in our db
     * @param permitDTO this is the object PermitDTO who is going to be created
     * @return PermitDTO after the creation, 
     * @throws ConflictException in case that the userId and albumId are already in another Permit
     * @throws BadRequestException in case that pass an error during the creation 
     */
    @Override
    public PermitDTO createPermit(PermitDTO permitDTO) {
        try {
            userService.getUserById(permitDTO.getUserId());
            albumService.getAlbumById(permitDTO.getAlbumId());
            Optional<Permit> isPermit = permitRepository.findByUserIdAndAlbumId(permitDTO.getUserId(), permitDTO.getAlbumId());
            if(isPermit.isPresent()) {
                throw new ConflictException(msg.msgConflict(OBJECT));
            }
            Permit permit = modMapper.map(permitDTO, Permit.class);
            Set<EPermission> permissions = permissionsMapped(permitDTO.getPermissions());
            permit.setPermissions(permissions);
            permit.setId(null);
            permitRepository.save(permit);
            PermitDTO response = modMapper.map(permit, PermitDTO.class);
            response.setPermissions(permitDTO.getPermissions());
            return response;
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }

    /**
     * This method is used to update a Permit in our db
     * @param permitId this is the id of the permit who is going to be updated
     * @param permitDTO this is the object PermitDTO who is going to be updated
     * @return PermitDTO after the update, 
     * @throws NotFoundException in case the permit is not created
     * @throws BadRequestException in case that pass an error during the update
     */
    @Override
    public PermitDTO updatePermit(Integer permitId, PermitDTO permitDTO) {
        try {
            userService.getUserById(permitDTO.getUserId());
            albumService.getAlbumById(permitDTO.getAlbumId());
            Optional<Permit> permit = permitRepository.findById(permitId);
            if (permit.isEmpty()) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, permitId));
            }
            Optional<Permit> isUserAndAlbum = permitRepository.findByUserIdAndAlbumId(permitDTO.getUserId(), permitDTO.getAlbumId());
            if(isUserAndAlbum.isPresent()&&(!Objects.equals(permitId, isUserAndAlbum.get().getId()))) {
                throw new ConflictException(msg.msgConflict(OBJECT));
            }
            permissionsMapped(permitDTO.getPermissions());
            Permit permitSave = modMapper.map(permitDTO, Permit.class);
            permitSave.setId(permitId);
            permitRepository.save(permitSave);
            return modMapper.map(permitSave, PermitDTO.class);
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
    }

    /**
     * This method is used to get a List Integer with the id of users by an album Id and a permission
     * @param albumId this is the id of the album to make the search
     * @param permission this is the permission to make the search
     * @return List Integer, with the id of the users found
     * @throws NotFoundException in case that the permission was not found
     * @throws BadRequestException in case that pass an error during the search
     */
    @Override
    public List<Integer> getUsersIdByAlbumIdAndPermission(Integer albumId, String permission) {
        try {
            List<Integer> usersId = new ArrayList<>();
            Set<String> permissionsSetString = new HashSet<>();
            permissionsSetString.add(permission);
             Set<EPermission> permissions = permissionsMapped(permissionsSetString);
            List<Permit> permitList = permitRepository.findByAlbumIdAndPermissionsIn(albumId, permissions);
            permitList.forEach(permit -> 
                usersId.add(permit.getUserId()
                )
            );
            return usersId;
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
        
    }
    
    /**
     * This method is used to get a List UserDTO by an album Id and a permission
     * @param albumId this is the id of the album to make the search
     * @param permission this is the permission to make the search
     * @return List UserDTO with the users found
     * @throws NotFoundException in case that the permission was not found
     * @throws BadRequestException in case that pass an error during the search
     */
    @Override
    public List<UserDTO> getUsersByAlbumIdAndPermission(Integer albumId, String permission) {
        try {
            List<UserDTO> users = new ArrayList<>();
            List<Integer> usersId = getUsersIdByAlbumIdAndPermission(albumId, permission);
            usersId.forEach(userId ->
                    users.add(userService.getUserById(userId)
                    )
            );
            return users;
        } catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
        
    }

    /**
     * This method is used to get a List with all the permits
     * @param pageNumber this is the number of the page
     * @param pageSize this is the size of the page
     * @return List PermitDTO with all the permits
     * @throws BadRequestException in case that pass an error during the searching
     */
    @Override
    public Page<PermitDTO> getAllPermits(Integer pageNumber, Integer pageSize) {
        try {
            Pageable paging = PageRequest.of(pageNumber, pageSize);
            Page<Permit> permits = permitRepository.findAll(paging);
            return permitsPageMapped(permits);
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get a List PermitDTO by the id of an user
     * @param userId this is the id of the user to make the search
     * @param pageNumber this is the number of the page
     * @param pageSize this is the size of the page
     * @return List PermitDTO with the permits found in the search by user Id
     * @throws BadRequestException in case that pass an error during the searching
     */
    @Override
    public Page<PermitDTO> getPermitsByUser(Integer userId, Integer pageNumber, Integer pageSize) {
        try {
            Pageable paging = PageRequest.of(pageNumber, pageSize);
            Page<Permit> permits = permitRepository.findByUserId(userId, paging);
            return permitsPageMapped(permits);
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get a List PermitDTO by the id of an album
     * @param albumId this is the id of the album to make the search
     * @param pageNumber this is the number of the page
     * @param pageSize this is the size of the page
     * @return List PermitDTO with the permits found in the search by album Id
     * @throws BadRequestException in case that pass an error during the searching
     */
    @Override
    public Page<PermitDTO> getPermitsByAlbum(Integer albumId, Integer pageNumber, Integer pageSize) {
        try {
            Pageable paging = PageRequest.of(pageNumber, pageSize);
            Page<Permit> permits = permitRepository.findByAlbumId(albumId, paging);
            return permitsPageMapped(permits);
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
        
    }

    /**
     * This method is used to delete a permit by id in our db
     * @param permitId this is the id of the Permit who is going to be deleted
     * @return String String with a message that the permit with id has been successfully deleted
     * @throws NotFoundException in case that the Permit was not been found with that id
     * @throws BadRequestException in case that pass an error during the delete, the method throw a 
     */
    @Override
    public String deletePermitById(Integer permitId) {
        try {
            if (permitRepository.findById(permitId).isEmpty()) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, permitId));
            }
            permitRepository.deleteById(permitId);
            return msg.msgSuccesfullyDelete(OBJECT, permitId);
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
    /**
     * This method is used to map a List of Permit on a List of PermitDTO
     * @param permits this is the List of Permit who is goig to be mapped
     * @return List PermitDTO with the permits mapped
     */
    private Page<PermitDTO> permitsPageMapped(Page<Permit> permits){
        List<PermitDTO> response = new ArrayList<>();
        permits.stream().map(permit -> modMapper.map(permit, PermitDTO.class)).forEachOrdered(response::add
        );
        return  new PageImpl<>(response);
    }
    
    /**
     * This method is used to map a Set of String on a Set of Permission
     * @param permissions this is the Set of Strings who is going to be mapped
     * @return Set Permission with the permissions mapped
     * @throws NotFoundException in case that at least one of the permissions send in the object PermitDTO had not been created
     */
    private Set<EPermission> permissionsMapped(Set<String> permissions) {
        Set<EPermission> response = new HashSet<>();
        permissions.stream().map(PermitServiceImp::isEnumPresent).map(en -> {
            if( en == null) {
                throw new NotFoundException(msg.msgNotHash(OBJECTPERMISSIONS, permissions));
            }
            return en;
        }).forEachOrdered(response::add
        );
        return response;
    }
}
