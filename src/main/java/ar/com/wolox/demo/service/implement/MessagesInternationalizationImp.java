/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.service.MessageInternationalization;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author C-Lug
 */
@Service
public class MessagesInternationalizationImp implements MessageInternationalization{
    
    @Autowired
    private MessageSource message;
    
    private static final String NOTFOUND = "throw.NotFoundException";
    private static final String NOTFOUNDHASH = "throw.NotFoundExceptionHash";
    private static final String BADREQUEST = "throw.BadRequestException";
    private static final String CONFLICT = "throw.ConflictException";
    private static final String SUCCESSDELETE = "delete.Successfully";
    private static final String CREATE = "create";
    private static final String SEARCH = "search";
    private static final String UPDATE = "update";
    private static final String DELETE = "delete";
    
    @Override
    public String msgBadRequestSearch(String object){
        String objectInt = message.getMessage(object, null, LocaleContextHolder.getLocale());
        String searchInt = message.getMessage(SEARCH, null, LocaleContextHolder.getLocale());
        return message.getMessage(BADREQUEST,new String[]{"1",objectInt,searchInt},LocaleContextHolder.getLocale());
    }
    
    @Override
    public String msgBadRequestCreate(String object){
        String objectInt = message.getMessage(object, null, LocaleContextHolder.getLocale());
        String createInt = message.getMessage(CREATE, null, LocaleContextHolder.getLocale());
        return message.getMessage(BADREQUEST,new String[]{"1",objectInt,createInt},LocaleContextHolder.getLocale());
    }
    
    @Override
    public String msgBadRequestUpdate(String object){
        String objectInt = message.getMessage(object, null, LocaleContextHolder.getLocale());
        String badRequestInt = message.getMessage(UPDATE, null, LocaleContextHolder.getLocale());
        return message.getMessage(BADREQUEST,new String[]{"1",objectInt,badRequestInt},LocaleContextHolder.getLocale());
    }
    
    @Override
    public String msgBadRequestDelete(String object){
        String objectInt = message.getMessage(object, null, LocaleContextHolder.getLocale());
        String deleteInt = message.getMessage(DELETE, null, LocaleContextHolder.getLocale());
        return message.getMessage(BADREQUEST,new String[]{"1",objectInt,deleteInt},LocaleContextHolder.getLocale());
    }
    
    @Override
    public String msgConflict(String object){
        String objectInt = message.getMessage(object, null, LocaleContextHolder.getLocale());
        return message.getMessage(CONFLICT,new String[]{"1",objectInt},LocaleContextHolder.getLocale());
    }
    
    @Override
    public String msgNotFound(String object, Integer objectId){
        String objectInt = message.getMessage(object, null, LocaleContextHolder.getLocale());
        String objectIdStr = objectId.toString();
        return message.getMessage(NOTFOUND,new String[]{"1",objectIdStr,objectInt},LocaleContextHolder.getLocale());
    }
    
    @Override
    public String msgSuccesfullyDelete(String object, Integer objectId){
        String objectInt = message.getMessage(object, null, LocaleContextHolder.getLocale());
        String objectIdStr = objectId.toString();
        return message.getMessage(SUCCESSDELETE,new String[]{"1",objectIdStr,objectInt},LocaleContextHolder.getLocale());
    }
    
    @Override
    public String msgNotHash(String object, Set<String> objectId){
        String objectInt = message.getMessage(object, null, LocaleContextHolder.getLocale());
        String objectIdStr = objectId.toString();
        return message.getMessage(NOTFOUNDHASH,new String[]{"1",objectIdStr,objectInt},LocaleContextHolder.getLocale());
    }
}
