/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service;

import java.util.Set;

/**
 *
 * @author C-Lug
 */
public interface MessageInternationalization {
    String msgBadRequestSearch(String object);
    String msgBadRequestCreate(String object);
    String msgBadRequestUpdate(String object);
    String msgBadRequestDelete(String object);
    String msgConflict(String object);
    String msgSuccesfullyDelete(String object, Integer objectId);
    String msgNotFound(String object, Integer objectId);
    String msgNotHash(String object, Set<String> value);
}
