/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service;

import ar.com.wolox.demo.dto.AlbumDTO;
import java.util.List;

/**
 *
 * @author C-Lug
 */
public interface AlbumService {
    
    List<AlbumDTO> getAllAlbums();
    List<AlbumDTO> getAlbumsByUser(Integer userId);
    AlbumDTO getAlbumById(Integer albumId);
    AlbumDTO createAlbum(AlbumDTO album);
    AlbumDTO updateAlbum(Integer albumId, AlbumDTO album);
    String deleteAlbumById(Integer albumId);
}
