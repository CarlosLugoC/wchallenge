 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.CommentDTO;
import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import org.springframework.stereotype.Service;
import ar.com.wolox.demo.service.CommentService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;

/**
 *
 * @author C-Lug
 */
@Service
public class CommentServiceImp  implements CommentService{

    private static final String URL = "https://jsonplaceholder.typicode.com/comments";
    private static final String URLUSER = "https://jsonplaceholder.typicode.com/users";
    private static final String OBJECT = "comment";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String STRINGFORMATEMAIL = "%s?email=%s";
    private static final String STRINGFORMATNAME = "%s?name=%s";
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private MessagesInternationalizationImp msg;
    /**
     * This method is used to get all the comments
     * @return List CommentDTO in case there were not found a comment, the method return an empty list
     * @throws BadRequestException in case that pass an error during the search
     */
    @Override
    public List<CommentDTO> getAllComments() {
        try {
            ResponseEntity<List<CommentDTO>> response = restTemplate.exchange(URL, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<CommentDTO>>() {});
            return response.getBody();
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get a commentDTO by the id
     * @param commentId this is the id to make the search for the comment
     * @return CommentDTO with the comment found
     * @throws NotFoundException in case there were not found a comment 
     */
    @Override
    public CommentDTO getCommentById(Integer commentId) {
        try {
            String url = String.format(STRINGFORMATSLASH,URL,commentId);
            ResponseEntity<CommentDTO> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<CommentDTO>() {});
            return response.getBody();
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, commentId));
        }
    }
    /**
     * This method is used to get a commentDTO by the name
     * @param name this is the name to make the search for the comment
     * @return CommentDTO with the comment found
     * @throws NotFoundException in case there were not found a comment  
     */
    @Override
    public List<CommentDTO> getCommentByName(String name) {
        try {
            String nameEncode = UriUtils.encodePath(name, "UTF-8");
            String url = String.format(STRINGFORMATNAME,URL,nameEncode);
            ResponseEntity<List<CommentDTO>> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<CommentDTO>>() {});
            return response.getBody();
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }
    
    @Override
    public List<CommentDTO> getCommentByNameV2(String name) {
        List<CommentDTO> commentsReponse = new ArrayList<>();
        List<CommentDTO> comments = getAllComments();
        comments.stream().filter(comment -> (comment.getName().equals(name))).forEachOrdered(
            commentsReponse::add
        );
        return commentsReponse;
    }
    
    /**
     * This method is used to get a List CommentDTO by the email
     * @param email this is the email to make the search for the comments
     * @return List CommentDTO in case there were not found a comment, the method return an empty List
     * @throws BadRequestException in case that pass an error during the search
     */
    @Override
    public List<CommentDTO> getCommentsByEmail(String email) {
        try {
            String url = String.format(STRINGFORMATEMAIL,URL,email);
            ResponseEntity<List<CommentDTO>> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<CommentDTO>>() {});
            return response.getBody();
        } catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }
    
    /**
     * This method is used to get a List CommentDTO by the id of an user
     * @param userId this is the id of the user to make the search
     * @return List CommentDTO with the comment found
     * @throws NotFoundException in case there were not found a comment 
     */
    @Override
    public List<CommentDTO> getCommentsByUserId(Integer userId) {
        try {
            String urlUser = String.format(STRINGFORMATSLASH,URLUSER,userId);
            ResponseEntity<UserDTO> responseUser = restTemplate.exchange(urlUser, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<UserDTO>() {});
            return getCommentsByEmail(responseUser.getBody().getEmail());
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to create a comment in the external api
     * @param comment this is the object CommentDTO who is going to be created
     * @return CommentDTO after to be created
     * @throws ConflictException in case to try create a comment with the same id
     * @throws BadRequestException in case to pass an error during the creation
     */
    @Override
    public CommentDTO createComment(CommentDTO comment) {
        try {
            if (getAllComments().contains(comment)) {
                throw new ConflictException(msg.msgConflict(OBJECT));
            }
            HttpEntity<CommentDTO> request = new HttpEntity<>(comment);
            ResponseEntity<CommentDTO> response = restTemplate.exchange(URL, HttpMethod.POST, request, new ParameterizedTypeReference<CommentDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }

    /**
     * This method is used to update a comment in the external api
     * @param commentId this is the id of the comment who is going to be updated
     * @param comment this is the object CommentDTO who is going to be update
     * @return CommentDTO after to be updated
     * @throws NotFoundException in case to try update a comment wich is not created
     * @throws BadRequestException in case to pass an error during the updating
     */
    @Override
    public CommentDTO updateComment(Integer commentId, CommentDTO comment) {
        try {
            if (getCommentById(commentId) == null) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, commentId));
            }
            String url =String.format(STRINGFORMATSLASH,URL,commentId);
            HttpEntity<CommentDTO> request = new HttpEntity<>(comment);
            ResponseEntity<CommentDTO> response = restTemplate.exchange(url, HttpMethod.PUT, request, new ParameterizedTypeReference<CommentDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
    }

    /**
     * This method is used to delete a comment by the id in the external api
     * @param commentId this is the id of the comment who is going to be deleted
     * @return String with a message that the comment with id has been successfully deleted
     * @throws NotFoundException in case to try delete a comment wich is not created
     * @throws BadRequestException in case to pass an error during the deleting
     */
    @Override
    public String deleteCommentById(Integer commentId) {
        try {
            if (getCommentById(commentId) == null) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, commentId));
            }
            String url = String.format(STRINGFORMATSLASH,URL,commentId);
            restTemplate.delete(url);
            return msg.msgSuccesfullyDelete(OBJECT, commentId);
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
}
