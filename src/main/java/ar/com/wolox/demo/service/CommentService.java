/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service;

import ar.com.wolox.demo.dto.CommentDTO;
import java.util.List;

/**
 *
 * @author C-Lug
 */
public interface CommentService {
    
    List<CommentDTO> getAllComments();
    CommentDTO getCommentById(Integer commentId);
    List<CommentDTO> getCommentByName(String name);
    List<CommentDTO> getCommentByNameV2(String name);
    List<CommentDTO> getCommentsByEmail(String email);
    List<CommentDTO> getCommentsByUserId(Integer userId);
    CommentDTO createComment(CommentDTO comment);
    CommentDTO updateComment(Integer commentId, CommentDTO comment);
    String deleteCommentById(Integer commentId);
}
