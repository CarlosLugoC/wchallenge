/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service;

import ar.com.wolox.demo.dto.UserDTO;
import java.util.List;

/**
 *
 * @author C-Lug
 */
public interface UserService {
    
    List<UserDTO> getAllUsers();
    UserDTO getUserById(Integer id);
    UserDTO createUser(UserDTO user);
    UserDTO updateUser(Integer userId, UserDTO user);
    String deleteUserById(Integer userId);
}
