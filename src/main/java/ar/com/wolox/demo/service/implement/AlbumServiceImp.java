/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.AlbumDTO;
import ar.com.wolox.demo.dto.PermitDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import ar.com.wolox.demo.repository.PermitRepository;
import ar.com.wolox.demo.service.AlbumService;
import ar.com.wolox.demo.service.PermitService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@Service
public class AlbumServiceImp  implements AlbumService{
    
    private static final String URL = "https://jsonplaceholder.typicode.com/albums";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String STRINGFORMATUSERID = "%s?userId=%s";
    private static final String OBJECT = "album";
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private PermitService permitService;
    
    @Autowired
    private PermitRepository permitRespository;

    @Autowired
    private MessagesInternationalizationImp msg;
    
    /**
     * This method is used to get all the albums in the external api
     * @return List AlbumDTO in case there were not found an album, the method return an empty list,
     * @throws BadRequestException in case that pass an error during the search
     */
    @Override
    public List<AlbumDTO> getAllAlbums() {
        try {
            ResponseEntity<List<AlbumDTO>> response = restTemplate.exchange(URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<AlbumDTO>>() {});
            return response.getBody();
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get all the albums of an user by the user id
     * @param userId this is the id of the user to make the search
     * @return List AlbumDTO, in case there were not found an album for that user, the method return an empty list,
     * @throws BadRequestException in case to pass an error during the search
     */
    @Override
    public List<AlbumDTO> getAlbumsByUser(Integer userId) {
        try {
            Integer pageSize = permitRespository.findAll().size();
            Integer pageNumber = 0;
            String url = String.format(STRINGFORMATUSERID,URL,userId);
            String permission = "READ";
            List<AlbumDTO> albumsResponse = new ArrayList<>();
            List<Integer> ids = new ArrayList<>();
            ResponseEntity<List<AlbumDTO>> responseApiExt = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<AlbumDTO>>() {});
            albumsResponse.addAll(responseApiExt.getBody());
            for (AlbumDTO albumDTO : albumsResponse) {
                ids.add(albumDTO.getId());
            }
            Page<PermitDTO> userPagePermits = permitService.getPermitsByUser(userId,pageNumber,pageSize);
            List<PermitDTO> userPermits = userPagePermits.getContent();
            for (PermitDTO permitDTO : userPermits) {
                if(permitDTO.getPermissions().contains(permission)){
                    AlbumDTO albumUser = getAlbumById(permitDTO.getAlbumId());
                    if(ids.contains(albumUser.getId())){
                        continue;
                    }
                    albumsResponse.add(albumUser);
                }
            }
            return albumsResponse;
        } catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get an album by the id
     * @param albumId this is the id of the album to make the search
     * @return AlbumDTO with the object searched
     * @throws NotFoundException in case there was not found an album for that id
     */
    @Override
    public AlbumDTO getAlbumById(Integer albumId) {
        try { 
            String url = String.format(STRINGFORMATSLASH,URL,albumId);
            ResponseEntity<AlbumDTO> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<AlbumDTO>() {});
            return response.getBody();
        } catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, albumId));
        }
    }

    /**
     * This method is used to create an album in the external api
     * @param album this is the object AlbumDTO who is going to be created
     * @return AlbumDTO after to be created
     * @throws ConflictException in case to try create an album with the same id   
     * @throws BadRequestException in case to pass an error during the creation
     */
    @Override
    public AlbumDTO createAlbum(AlbumDTO album) {
        try {
            if (getAllAlbums().contains(album)) {
                throw new ConflictException(msg.msgConflict(OBJECT));
            }
            HttpEntity<AlbumDTO> request = new HttpEntity<>(album);
            ResponseEntity<AlbumDTO> response = restTemplate.exchange(URL, HttpMethod.POST, request, new ParameterizedTypeReference<AlbumDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }

    /**
     * This method is used to update an album in the external api
     * @param albumId this is the id of the album who is going to be updated
     * @param album this is the object AlbumDTO who is going to be update
     * @return AlbumDTO after to be updated
     * @throws NotFoundException in case to try update an album wich is not created
     * @throws BadRequestException in case to pass an error during the updating
     */
    @Override
    public AlbumDTO updateAlbum(Integer albumId, AlbumDTO album) {
        try {
            if (getAlbumById(albumId) == null) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, albumId));
            }
            String url = String.format(STRINGFORMATSLASH,URL,albumId);
            HttpEntity<AlbumDTO> request = new HttpEntity<>(album);
            ResponseEntity<AlbumDTO> response = restTemplate.exchange(url, HttpMethod.PUT, request, new ParameterizedTypeReference<AlbumDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
    }

    /**
     * This method is used to delete an album by the id in the external api
     * @param albumId this is the id of the album who is going to be deleted
     * @return String with a message that the album with id has been successfully deleted
     * @throws NotFoundException in case to try delete an album wich is not created
     * @throws BadRequestException in case to pass an error during the deleting 
     */
    @Override
    public String deleteAlbumById(Integer albumId) {
        try {
            if (getAlbumById(albumId) == null) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, albumId));
            }
            String url = String.format(STRINGFORMATSLASH,URL,albumId);
            restTemplate.delete(url);
            return msg.msgSuccesfullyDelete(OBJECT, albumId);
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
}
