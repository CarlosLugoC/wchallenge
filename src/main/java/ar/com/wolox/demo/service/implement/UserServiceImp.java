/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.UserDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import ar.com.wolox.demo.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@Service
public class UserServiceImp implements UserService{

    private static final String URL = "https://jsonplaceholder.typicode.com/users";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String OBJECT = "user";
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private MessagesInternationalizationImp msg;
    /**
     * This method is used to get all the users from the external api
     * @return List UserDTO, in case there were not found an album return an empty List,
     * @throws BadRequestException in case that pass an error during the search
     */
    @Override
    public List<UserDTO> getAllUsers() {
        try {
            ResponseEntity<List<UserDTO>> response = restTemplate.exchange(
                    URL, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<List<UserDTO>>() {});
            return response.getBody();
        } catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get an user by the id
     * @param id this is the id of the user who is going to be search
     * @return UserDTO with the user found by id 
     * @throws NotFoundException in case there were not found an user with that id
     */
    @Override
    public UserDTO getUserById(Integer id) {
        try {
            String url = String.format(STRINGFORMATSLASH,URL,id);
            ResponseEntity<UserDTO> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY,new ParameterizedTypeReference<UserDTO>() {});
            return response.getBody();
        }catch(RestClientException ex){
            throw new NotFoundException(msg.msgNotFound(OBJECT, id));
        }
    }

    /**
     * This method is used to create an user in the external api
     * @param user this is the object UserDTO whor is going to be created
     * @return UserDTO after the creation, 
     * @throws ConflictException in case that the user is alredy created
     * @throws BadRequestException in case that pass an error during the creation
     */
    @Override
    public UserDTO createUser(UserDTO user) {
        try {
            if (getAllUsers().contains(user)) {
                throw new ConflictException(msg.msgConflict(OBJECT));
            }
            String url = URL;
            HttpEntity<UserDTO> request = new HttpEntity<>(user);
            ResponseEntity<UserDTO> response = restTemplate.exchange(url, HttpMethod.POST, request, new ParameterizedTypeReference<UserDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }

    /**
     * This method is used to update an user in the external api
     * @param userId this is the id of the user who is going to be updated
     * @param user this is the object UserDTO who has the information of the user to update
     * @return UserDTO after the update
     * @throws NotFoundException in case that the user to modify is not created
     * @throws BadRequestException in case that pass an error during the update
     */
    @Override
    public UserDTO updateUser(Integer userId, UserDTO user) {
        try {
            if (getUserById(userId) == null) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, userId));
            }
            String url =String.format(STRINGFORMATSLASH,URL,userId);
            HttpEntity<UserDTO> request = new HttpEntity<>(user);
            ResponseEntity<UserDTO> response = restTemplate.exchange(url, HttpMethod.PUT, request,  new ParameterizedTypeReference<UserDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
    }

    /**
     * This method is used to delete an user by the id in the external api
     * @param userId this is the id of the user who is going to be deleted
     * @return String with the message that the user has been successfully deleted
     * @throws NotFoundException in case that the user to delete is not created
     * @throws BadRequestException in case that pass an error during the delete
     */
    @Override
    public String deleteUserById(Integer userId) {
        try {
            if (getUserById(userId) == null) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, userId));
            }
            String url = String.format(STRINGFORMATSLASH,URL,userId);
            restTemplate.delete(url);
            return msg.msgSuccesfullyDelete(OBJECT, userId);
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
    
}
