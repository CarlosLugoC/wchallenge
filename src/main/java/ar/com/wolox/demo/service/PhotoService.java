/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service;

import ar.com.wolox.demo.dto.PhotoDTO;
import java.util.List;

/**
 *
 * @author C-Lug
 */
public interface PhotoService {
    
    List<PhotoDTO> getAllPhotos();
    List<PhotoDTO> getPhotosByAlbumId(Integer albumId);
    List<PhotoDTO> getPhotosByUser(Integer userId);
    PhotoDTO getPhotoById(Integer photoId);
    PhotoDTO createPhoto(PhotoDTO photo);
    PhotoDTO updatePhoto(Integer photoId, PhotoDTO photo);
    String deletePhotoById(Integer photoId);
}
