/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service;

import ar.com.wolox.demo.dto.PostDTO;
import java.util.List;

/**
 *
 * @author C-Lug
 */
public interface PostService {
    
    List<PostDTO> getAllPosts();
    PostDTO getPostById(Integer id);
}
