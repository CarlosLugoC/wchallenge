/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service;

import ar.com.wolox.demo.dto.PermitDTO;
import ar.com.wolox.demo.dto.UserDTO;
import java.util.List;
import org.springframework.data.domain.Page;

/**
 *
 * @author C-Lug
 */
public interface PermitService {
    
    PermitDTO createPermit(PermitDTO permission);
    PermitDTO updatePermit(Integer permissionId, PermitDTO permission);
    List<Integer> getUsersIdByAlbumIdAndPermission(Integer albumId, String permission);
    List<UserDTO> getUsersByAlbumIdAndPermission(Integer albumId, String permission);
    Page<PermitDTO> getAllPermits(Integer pageNumber, Integer pageSize);
    Page<PermitDTO> getPermitsByUser(Integer userId, Integer pageNumber, Integer pageSize);
    Page<PermitDTO> getPermitsByAlbum(Integer albumId, Integer pageNumber, Integer pageSize);
    String deletePermitById(Integer permissionId);
}
