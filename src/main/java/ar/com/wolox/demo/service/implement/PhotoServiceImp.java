/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.service.implement;

import ar.com.wolox.demo.dto.AlbumDTO;
import ar.com.wolox.demo.dto.PhotoDTO;
import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import ar.com.wolox.demo.service.AlbumService;
import ar.com.wolox.demo.service.PhotoService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author C-Lug
 */
@Service
public class PhotoServiceImp implements PhotoService{

    private static final String URL = "https://jsonplaceholder.typicode.com/photos";
    private static final String STRINGFORMATSLASH = "%s/%s";
    private static final String STRINGFORMATALBUM = "%s?albumId=%s";
    private static final String OBJECT = "photo";
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private AlbumService albumService;
    
    @Autowired
    private MessagesInternationalizationImp msg;
    
    /**
     * This method is used to get all the phoitos in the external api
     * @return List PhotoDTO in case there were not found a photo, the method return an empty list,
     * @throws BadRequestException in case that pass an error during the creation
     */
    @Override
    public List<PhotoDTO> getAllPhotos() {
        try {
            ResponseEntity<List<PhotoDTO>> response = restTemplate.exchange(URL, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<PhotoDTO>>() {});
            return response.getBody();
        }catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get a List PhotoDTO by the id of an album
     * @param albumId this is the id of an album to make the search
     * @return List PhotoDTO in case there were not found a photo for that album, the method return an empty List,
     * @throws BadRequestException in case that pass an error during the search 
     */
    @Override
    public List<PhotoDTO> getPhotosByAlbumId(Integer albumId) {
        try {
            String url = String.format(STRINGFORMATALBUM,URL,albumId);
            ResponseEntity<List<PhotoDTO>> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<List<PhotoDTO>>() {});
            return response.getBody();
        }catch(RestClientException ex){
            throw new BadRequestException(msg.msgBadRequestSearch(OBJECT));
        }
    }

    /**
     * This method is used to get a List PhotoDTO by the id of an user
     * @param userId this is the id of an user to make the search
     * @return List PhotoDTO with the photos found by user
     * @throws NotFoundException in case there were not found a photo for that user
     */
    @Override
    public List<PhotoDTO> getPhotosByUser(Integer userId) {
        List<PhotoDTO> photosUser = new ArrayList<>();
        List<AlbumDTO> albums = albumService.getAlbumsByUser(userId);
        if(albums.isEmpty()) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, userId));
        }
        List<PhotoDTO> allPhotos = getAllPhotos();
        allPhotos.forEach(photo -> 
                albums.stream().filter(album -> (photo.getAlbumId().equals(album.getId()))).forEachOrdered(item -> 
                        photosUser.add(photo)
                )
        );
        return photosUser;
    }

    /**
     * This method is used to get a photo by the id
     * @param photoId this is the id of the photo to make the search
     * @return PhotoDTO with the photo found by id
     * @throws NotFoundException in case there was not found a photo for that id 
     */
    @Override
    public PhotoDTO getPhotoById(Integer photoId) {
        try {
            String url = String.format(STRINGFORMATSLASH,URL,photoId); 
            ResponseEntity<PhotoDTO> response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<PhotoDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new NotFoundException(msg.msgNotFound(OBJECT, photoId));
        }
    }

    /**
     * This method is used to create a photo in the external api
     * @param photo this is the object PhotoDTO who is going to be created
     * @return PhotoDTO after to be created
     * @throws ConflictException in case to try create a photo with the same id 
     * @throws BadRequestException in case to pass an error during the creation
     */
    @Override
    public PhotoDTO createPhoto(PhotoDTO photo) {
        try {
            if (getAllPhotos().contains(photo)) {
                throw new ConflictException(msg.msgConflict(OBJECT));
            }
            HttpEntity<PhotoDTO> request = new HttpEntity<>(photo);
            ResponseEntity<PhotoDTO> response = restTemplate.exchange(URL, HttpMethod.POST, request, new ParameterizedTypeReference<PhotoDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestCreate(OBJECT));
        }
    }

    /**
     * This method is used to update a photo in the external api
     * @param photoId this is the id of the photo who is going to be updated
     * @param photo this is the object PhotoDTO who is going to be update
     * @return PhotoDTO after to be updated
     * @throws NotFoundException in case to try update a photo wich is not created
     * @throws BadRequestException in case to pass an error during the updating
     */
    @Override
    public PhotoDTO updatePhoto(Integer photoId, PhotoDTO photo) {
        try {
            if (getPhotoById(photoId) == null) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, photoId));
            }
            String url = String.format(STRINGFORMATSLASH,URL,photoId);
            HttpEntity<PhotoDTO> request = new HttpEntity<>(photo);
            ResponseEntity<PhotoDTO> response = restTemplate.exchange(url, HttpMethod.PUT, request, new ParameterizedTypeReference<PhotoDTO>() {});
            return response.getBody();
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestUpdate(OBJECT));
        }
    }

    /**
     * This method is used to delete a photo by the id in the external api
     * @param photoId this is the id of the photo who is going to be deleted
     * @return String with a message that the photo with id has been successfully deleted
     * @throws NotFoundException in case to try delete a photo wich is not created
     * @throws BadRequestException in case to pass an error during the deleting
     */
    @Override
    public String deletePhotoById(Integer photoId) {
        try {
            if (getPhotoById(photoId) == null) {
                throw new NotFoundException(msg.msgNotFound(OBJECT, photoId));
            }
            String url = String.format(STRINGFORMATSLASH,URL,photoId);
            restTemplate.delete(url);
            return msg.msgSuccesfullyDelete(OBJECT, photoId);
        }catch(RestClientException ex) {
            throw new BadRequestException(msg.msgBadRequestDelete(OBJECT));
        }
    }
    
}
