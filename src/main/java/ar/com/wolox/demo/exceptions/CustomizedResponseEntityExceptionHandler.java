/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.exceptions;

import ar.com.wolox.demo.exceptions.responses.BadRequestException;
import ar.com.wolox.demo.exceptions.responses.ConflictException;
import ar.com.wolox.demo.exceptions.responses.NotFoundException;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author C-Lug
 */
@ControllerAdvice
@RestController
@Slf4j
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{
    
    /**
     * This method is used to handle notFoundException
     * @param ex this is the notFoundException
     * @param request this is a request from the web
     * @return ResponseEntity with the status NOT_FOUND and ExceptionResponse who has date, message and description 
     */
    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ExceptionResponse> handleNotFoundException(NotFoundException ex, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
    }
    
    /**
     * This method is used to handle badRequestException
     * @param ex this is the badRequestException
     * @param request this is a request from the web
     * @return ResponseEntity with the status BAD_REQUEST and ExceptionResponse who has date, message and description 
     */
    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<ExceptionResponse> handleBadRequestException(BadRequestException ex, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }
    
    /**
     * This method is used to handle createdException
     * @param ex this is the createdException
     * @param request this is a request from the web
     * @return ResponseEntity with the status CREATED and ExceptionResponse who has date, message and description 
     */
    @ExceptionHandler(ConflictException.class)
    public final ResponseEntity<ExceptionResponse> handleConflictException(ConflictException ex, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse,HttpStatus.CONFLICT);
    }
}
