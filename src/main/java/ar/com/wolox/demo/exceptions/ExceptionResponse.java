/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.exceptions;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author C-Lug
 */
@Data
@AllArgsConstructor
public class ExceptionResponse {
    
    private Date timeStamp;
    private String message;
    private String detail;
}
