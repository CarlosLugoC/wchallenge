/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo.repository;

import ar.com.wolox.demo.model.EPermission;
import ar.com.wolox.demo.model.Permit;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author C-Lug
 */
@Repository
public interface PermitRepository extends JpaRepository<Permit, Integer>{
    
    /**
     * This method is used to search a Permit by the id of an user and the id of an album in the database
     * @param userId this is the id of the user to make the search of the permit
     * @param albumId this is the id of the album to make the search of the permit
     * @return Optional Permit, in case that not Found a Permit with the userId and the albumId this would be empty
     */
    Optional<Permit> findByUserIdAndAlbumId(Integer userId, Integer albumId);
    
    /**
     * This method is used to search a List Permit by the id of an album and the permission to write in the album in the database
     * @param albumId this is the id of the album to make the search of the List Permit
     * @param permissions this is the value of the permission to make the search
     * @return List Permit, in case that not found a Permit, the method return an empty List
     */
    List<Permit> findByAlbumIdAndPermissionsIn(Integer albumId, Set<EPermission> permissions);
    
    /**
     * This method is used to seach a List Permit by the id of an album in the database
     * @param albumId this is the id of the album to make the search of the List Permit
     * @param pageable
     * @return List Permit, in case that not found a Permit, the method return an empty List
     */
    Page<Permit> findByAlbumId(Integer albumId, Pageable pageable);
    
    /**
     * This method is used to seach a List Permit by the id of an user in the database
     * @param userId this is the id of the user to make the search of the List Permit
     * @param pageable
     * @return List Permit, in case that not found a Permit, the method return an empty List
     */
    Page<Permit> findByUserId(Integer userId,  Pageable pageable);
}
