/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.wolox.demo;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author C-Lug
 */
@EnableSwagger2
@Configuration
public class ConfigurationW {
        
        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }   
        
        @Bean
        public ModelMapper modelMapper() {
            return new ModelMapper();
        }
        
         @Bean
        public Docket api() {
            return new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .select()
                    .paths(PathSelectors.regex("/.*"))
                    .apis(RequestHandlerSelectors
                        .basePackage("ar.com.wolox.demo.controller"))
                    .build()
                    .useDefaultResponseMessages(false);
        }

        private ApiInfo apiInfo() {
             return new ApiInfoBuilder()
                     .title("W Challenge")
                     .version("1.0")
                     .description("Project For Wolox")
                     .contact(new Contact("Carlos Lugo Cabrera", "", ""))
                     //.license("Apache License Version 2.0")
                     .build();
         }
    
        @Bean
        public ResourceBundleMessageSource messageSource() {

            var source = new ResourceBundleMessageSource();
            source.setBasenames("lang/messages");
            source.setUseCodeAsDefaultMessage(true);

            return source;
        }

}
