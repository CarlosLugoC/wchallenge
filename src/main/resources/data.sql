/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  C-Lug
 * Created: 25/10/2020
 */
 


INSERT INTO permits (album_id, user_id) VALUES
  (1, 1),
  (1, 2),
  (2, 1),
  (2, 2),
  (3, 1),
  (4, 2),
  (5, 2),
  (1, 3),
  (1, 4);
INSERT INTO permit_permissions (permit_id, permissions) VALUES
  (1, 'WRITE'),
  (1, 'READ'),
  (2, 'WRITE'),
  (2, 'READ'),
  (3, 'READ'),
  (4, 'WRITE'),
  (4, 'READ'),
  (5, 'WRITE'),
  (6, 'READ'),
  (7, 'READ'),
  (8, 'READ'),
  (9, 'WRITE');
